using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;

public class DebugBoxCast : MonoBehaviour
{
    [SerializeField]
    MapGrids mapGrid;
    [SerializeField]
    QuadrantGrid quadrantGrid;
    [SerializeField]
    Transform targetPos;
    [SerializeField]
    float radius;

    private void OnDrawGizmos()
    {
        NativeList<int> temp = new NativeList<int>(Allocator.Temp);
        quadrantGrid.Settings.RaycastAll(transform.position.XZ(), targetPos.position.XZ(), temp);
        temp.Dispose();

        /*
        quadrantGrid.Settings.BoxcastEdge(transform.position.XZ(), radius, out var grids, Allocator.Temp);
        foreach (var gridIndex in grids)
        {
            var pos = quadrantGrid.Settings.GetGridPosition(gridIndex);
            Debug.DrawRectangle(pos.X0Y(), Vector2.one, Color.green);
        }
        Debug.DrawCircle(transform.position, radius, Color.blue);
        grids.Dispose();
        */
    }
}
