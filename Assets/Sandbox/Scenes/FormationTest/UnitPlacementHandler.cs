using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;
using System.Linq;
using Unity.Jobs;
using Unity.Collections;
using Gtion.Plugin.DI;

public struct ArrangementResult
{
    public float2[] placement;
    public HashSet<int> finishPoint;
}

public static class UnitPlacementHandler
{
    struct LineType
    {
        public bool IsEmpty => spaceSize == 0;

        public float2 startPosition;
        public float spaceSize;
    }

    public static Dictionary<UnitSizeType, float2> editorCache = new Dictionary<UnitSizeType, float2>()
    {
        { UnitSizeType.Infantry , new float2(1,1) },
        { UnitSizeType.Tank , new float2(2,3) },

    };

    public static ArrangementResult ArrangePlacement(GridSettings settings, UnitSizeType[] units, Vector2 offsetPosition, Vector2 direction)
    {
        IReadOnlyDictionary<UnitSizeType, float2> CodeToSize;
        if (Application.isPlaying)
        {
            GDi.Load(out UnitSizeGroupingConfig config);
            CodeToSize = config;
        }
        else
        {
            CodeToSize = editorCache;
        }
        return ArrangePlacement(settings, units, offsetPosition, direction, CodeToSize);
    }
    public static ArrangementResult ArrangePlacement(GridSettings settings, UnitSizeType[] units, Vector2 offsetPosition, Vector2 direction, IReadOnlyDictionary<UnitSizeType, float2> CodeToSize) 
    {
        float2 worldSize = (((Vector2)settings.gridSize * settings.nodeSize) / 2) - new Vector2(settings.nodeSize * 0.5f, settings.nodeSize * 0.5f);
        float2 worldSizeOffset = worldSize;// - new float2(settings.nodeSize*0.5f , settings.nodeSize * 0.5f);

        float2 averageSize = float2.zero;
        float highestWidth = 0;
        for (int i = 0; i < units.Length; i++)
        {
            averageSize += CodeToSize[units[i]];
            if (highestWidth < CodeToSize[units[i]].x)
            {
                highestWidth = CodeToSize[units[i]].x;
            }
        }

        averageSize = averageSize / units.Length;
        float2 normalAverage = averageSize / Mathf.Min(averageSize.x, averageSize.y);
        float blockSize = normalAverage.x * normalAverage.y;
        int blockHeightWidthNormal = Mathf.CeilToInt(math.sqrt(units.Length / blockSize));//basically rectangle

        int blockWidthCount = Mathf.CeilToInt(blockHeightWidthNormal * normalAverage.y);
        float normalWidth = blockWidthCount * averageSize.x;
        float blockWidth = normalWidth; ///*normalWidth + */(Mathf.Floor(normalWidth / highestWidth) * highestWidth) ; //(normalWidth % -highestWidth);
        //Debug.Log($"{math.sqrt(units.Length / blockSize)} {blockHeightWidthNormal} - {blockWidthCount} - {normalWidth} - {blockWidth}");

        int blockHeightCount = Mathf.RoundToInt(blockHeightWidthNormal * normalAverage.x);
        float blockHeight = blockHeightCount * averageSize.y;

        float2 blockCenter = new float2(-blockWidth, blockHeight) / 2;
        worldSizeOffset -= new float2(blockWidth, blockHeight)/2;
        offsetPosition = new float2(math.clamp(offsetPosition.x, -worldSizeOffset.x, worldSizeOffset.x), 
                                    math.clamp(offsetPosition.y, -worldSizeOffset.y, worldSizeOffset.y));

        HashSet<int> indexGridTarget = new HashSet<int>();
        float2[] arrangeResult = new float2[units.Length];
        Dictionary<UnitSizeType, LineType> inProgressLines = new();
        float latestHeight = blockCenter.y;
        for (int i = 0; i < units.Length; i++)
        {
            inProgressLines.TryGetValue(units[i], out LineType selectedLine);
            if (selectedLine.IsEmpty)
            {
                latestHeight -= CodeToSize[units[i]].y / 2;
                float offset = (blockWidth % CodeToSize[units[i]].x) / 2;
                selectedLine = new LineType()
                {
                    startPosition = new float2(blockCenter.x + offset, latestHeight),
                    spaceSize = blockWidth
                };
                inProgressLines.Add(units[i] , selectedLine);
                latestHeight -= CodeToSize[units[i]].y/2;
            }

            float2 localPosition = selectedLine.startPosition + new float2(CodeToSize[units[i]].x/2 + (blockWidth - selectedLine.spaceSize), 0);
            if (direction.y < 0)
            {
                direction = -direction; //mirror
            }

            Vector2 worldDirection = Quaternion.AngleAxis(Vector2.SignedAngle(Vector2.up, localPosition), -Vector3.forward) * direction;
            var targetPosition = offsetPosition + (worldDirection * math.length(localPosition));
            targetPosition = new float2(math.clamp(targetPosition.x, -worldSize.x, worldSize.x), math.clamp(targetPosition.y, -worldSize.y, worldSize.y));
            arrangeResult[i] = targetPosition;

            int gridIndex = settings.GetNodeIndex(targetPosition);
            if (!indexGridTarget.Contains(gridIndex))
            {
                indexGridTarget.Add(gridIndex);
            }
            
            selectedLine.spaceSize -= CodeToSize[units[i]].x;
            if (selectedLine.spaceSize < CodeToSize[units[i]].x)
            {
                inProgressLines.Remove(units[i]);
            }
            else
            {
                inProgressLines[units[i]] = selectedLine;
            }
        }

        return new ArrangementResult() 
        {
            placement = arrangeResult,
            finishPoint = indexGridTarget
        };
    }

}
