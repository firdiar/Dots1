using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using Gtion.Plugin.DI;
using System.Threading.Tasks;

public interface IHaveSize
{
    float2 GetSize { get; }
}


public class SetFormationDebug : MonoBehaviour
{
    [RequireType(typeof(IHaveSize))]
    [SerializeField]
    Object prefabs;

    [Header("Debug")]
    int size = 20;

    [SerializeField]
    Packing packing;

    //DI
    [GInject]
    IPlayerInput playerInput;

    private void Start()
    {
        GDi.Request(this , OnReady);
    }

    void OnReady() 
    {
        playerInput.OnClick += (pos, state) => 
        {
            if (state != ButtonState.Down) return;
            SetFormations(pos);
        };
    }

    private async void SetFormations(Vector2 position) 
    {
        List<Pack> boxes = new List<Pack>();
        for (int i = 0; i < size; i++)
        {
            var obj = Instantiate(prefabs, position.X1Y(), VectorExtension.Quaternion2DZ, null) as Component;
            boxes.Add(new Pack() { size = (obj as IHaveSize).GetSize , obj = obj , position = Vector2.one*100 });
        }

        boxes.Sort((a, b) => 
        {
            return (int)Mathf.Sign(b.size.magnitude-a.size.magnitude);
        });

        packing = new Packing(new Pack()
        {
            size = Vector2.one * 30,
            position = position
        } , boxes);

        foreach (var box in boxes)
        {
            box.obj.transform.position = box.position.X1Y();
        }

        await packing.Execute();
    }

    private void Update()
    {
        foreach (var pack in packing.availableSlotRight)
        {
            Debug.DrawRectangle(pack.position.X1Y() + (pack.size/2).X1Y(), pack.size, Color.green);
        }

        foreach (var pack in packing.availableSlotTop)
        {
            Debug.DrawRectangle(pack.position.X1Y() + (pack.size / 2).X1Y(), pack.size, Color.green);
        }

    }
}

[System.Serializable]
public struct Pack
{
    public Vector2 size;
    public Vector2 position;
    public string id;
    public Component obj;
}

[System.Serializable]
public class Packing
{
    const float PackMinSize = 1;
    const float PackOffset = 0.25f;

    [SerializeField]
    public List<Pack> availableSlotRight = new List<Pack>();
    [SerializeField]
    public List<Pack> availableSlotTop = new List<Pack>();
    [SerializeField]
    List<Pack> boxes = new List<Pack>();

    public Packing(Pack pack, List<Pack> orders)
    {
        availableSlotTop.Add(pack);
        boxes = orders;
    }

    public async Task Execute() 
    {
        for (int i = 0; i < boxes.Count; i++)
        {
            int targetSlot = 0;
            bool isFoundRight = false;
            for (int j = 0; j < availableSlotRight.Count; j++)
            {
                if (IsFit(availableSlotRight[j], boxes[i]))
                {
                    targetSlot = j;
                    isFoundRight = true;
                    break;
                }
            }

            if (!isFoundRight)
            {
                for (int j = 0; j < availableSlotTop.Count; j++)
                {
                    if (IsFit(availableSlotTop[j], boxes[i]))
                    {
                        targetSlot = j;
                        break;
                    }
                }
            }

            await System.Threading.Tasks.Task.Delay(500);
            
            Debug.Log("Get Slot : "+targetSlot);
            await PutIn(targetSlot, isFoundRight ? availableSlotRight : availableSlotTop, boxes[i]);
        }
    }

    public async Task PutIn(int target, List<Pack> avaSlot, Pack box)
    {
        var slot = avaSlot[target];
        box.position = slot.position;
        await System.Threading.Tasks.Task.Delay(500);

        box.obj.transform.position = box.position.X1Y() + (box.size / 2).X1Y();

        //create sub top]
        bool executeTop = slot.size.y - box.size.y - PackOffset >= PackMinSize;
        bool executeRight = slot.size.x - box.size.x - PackOffset >= PackMinSize;

        //create sub right
        if (executeRight) //just create new pack only if it is quite big
        {
            float packHeight;
            if (executeTop)
            {
                packHeight = box.size.y;
            }
            else
            {
                packHeight = slot.size.y;
            }

            var topPack = new Pack()
            {
                size = new Vector2(slot.size.x - box.size.x - PackOffset, packHeight),
                position = new Vector2(slot.position.x + box.size.x + PackOffset, slot.position.y), //go right
                id = "Right"
            };
            availableSlotRight.Add(topPack);
        }
        //await System.Threading.Tasks.Task.Delay(500);

        if (executeTop) //just create new pack only if it is quite big
        {
            var topPack = new Pack()
            {
                size = new Vector2(slot.size.x , slot.size.y - box.size.y - PackOffset),
                position = new Vector2(slot.position.x, slot.position.y + box.size.y+ PackOffset), //go up
                id = "Top"
            };
            availableSlotTop.Add(topPack);
        }
        //await System.Threading.Tasks.Task.Delay(500);


        avaSlot.RemoveAt(target);
    }

    public bool IsFit(Pack slot, Pack box)
    {
        return slot.size.x >= box.size.x && slot.size.y >= box.size.y;
    }
}
