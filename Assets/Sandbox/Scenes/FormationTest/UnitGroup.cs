using System.Collections;
using System.Collections.Generic;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;

public struct UnitGroup
{
    public NativeArray<Unit> units;
    public Quaternion quaternion;
}

public struct UnitTransform
{
    public float2 size;
    public float2 position;
}

public struct Unit
{
    public UnitTransform transform;
    public Entity entity;
}

[BurstCompile]
public struct UnitGroupingJob : IJob
{
    public float minGroupSize;
    public float2 spacing;

    public NativeArray<Unit> units;
    public UnitTransform groups;

    public void Execute()
    {
        NativeList<UnitTransform> availableSlotTop = new NativeList<UnitTransform>(15, Allocator.Temp);
        NativeList<UnitTransform>  availableSlotRight = new NativeList<UnitTransform>(15, Allocator.Temp) { groups };

        for (int i = 0; i < units.Length; i++)
        {
            bool isFoundPosition = false;
            for (int j = 0; j < availableSlotRight.Length; j++)
            {
                isFoundPosition = IsFit(availableSlotRight[j], units[i]);
                if (isFoundPosition)
                {
                    PutIn(true, availableSlotTop, availableSlotRight, j, i);
                    break;
                }
            }
            if (isFoundPosition) continue;

            for (int j = 0; j < availableSlotTop.Length; j++)
            {
                isFoundPosition = IsFit(availableSlotTop[j], units[i]);
                if (isFoundPosition)
                {
                    PutIn(false, availableSlotTop, availableSlotRight, j, i);
                    break;
                }
            }
        }

        availableSlotTop.Dispose();
        availableSlotRight.Dispose();
    }

    public void PutIn(bool isRight, NativeList<UnitTransform> top, NativeList<UnitTransform> right, int slotIndex, int unitIndex)
    {
        NativeList<UnitTransform> availableSlots =isRight ? right : top;

        var slot = availableSlots[slotIndex];
        var unit = units[unitIndex];
        unit.transform.position = slot.position;
        units[unitIndex] = unit;

        var unitT = unit.transform;

        //are we going to make sub slot for each new Area
        float2 unitSpace = new float2(unitT.size.x + spacing.x, unitT.size.y + spacing.y);

        float rightWidth = slot.size.x - unitSpace.x;
        bool executeRight = rightWidth >= minGroupSize;

        float topHeight = slot.size.y - unitSpace.y;
        bool executeTop = topHeight >= minGroupSize;


        //create sub right
        if (executeRight)
        {
            float groupHeight;
            if (executeTop)
            {
                groupHeight = unitT.size.y;
            }
            else
            {
                groupHeight = slot.size.y;
            }

            var rightGroup = new UnitTransform()
            {
                size = new float2(rightWidth, groupHeight),
                position = new float2(slot.position.x + unitSpace.x, slot.position.y)
            };
            right.Add(rightGroup);
        }

        if (executeTop) //just create new pack only if it is quite big
        {
            var topGroup = new UnitTransform()
            {
                size = new float2(slot.size.x, topHeight),
                position = new float2(slot.position.x, slot.position.y + unitSpace.y)
            };
            top.Add(topGroup);
        }
        //await System.Threading.Tasks.Task.Delay(500);


        availableSlots.RemoveAt(slotIndex);
    }

    public bool IsFit(UnitTransform slot, Unit box)
    {
        return slot.size.x >= box.transform.size.x && slot.size.y >= box.transform.size.y;
    }
}