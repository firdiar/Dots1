using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using Gtion.Plugin.DI;
using System.Threading.Tasks;
using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;
using Unity.Jobs;
using NaughtyAttributes;

public class SetFormationDebug2 : MonoBehaviour
{
    [RequireType(typeof(IHaveSize))]
    [SerializeField]
    Object prefabs;

    [SerializeField]
    GameObject unitPrefab;

    [Header("Debug")]
    int size = 20;

    //DI
    [GInject]
    IPlayerInput playerInput;
    [GInject]
    ECSInstantiate instantiator;

    private void Start()
    {
        GDi.Request(this, OnReady);        
    }
    [SerializeField]
    GridSettings settings;

    [SerializeField]
    UnitSizeType[] unitCodes;
    [SerializeField]
    UnitSizeGroupingConfig sizeDictionary;

    [SerializeField]
    float2[] placements;

    [SerializeField]
    Vector2 direction;
    [SerializeField]
    Vector2 normDirection;

    private void OnValidate()
    {
        normDirection = direction.normalized;
    }

    [Button]
    void LogPlacement() 
    {
        placements = UnitPlacementHandler.ArrangePlacement(settings , unitCodes , Vector2.zero , normDirection, sizeDictionary).placement;
    }

    private void OnDrawGizmos()
    {
        for (int i = 0; i < unitCodes.Length; i++)
        {
            Color col = Color.white * ((i%5) / (5 * 1.0f));
            col.a = 1;
            Debug.DrawRectangle(placements[i].X0Y(), sizeDictionary[unitCodes[i]], normDirection, col );
        }

        /*
        Vector3 axis = Vector3.Cross(originalVector, Vector3.forward);
        Vector2 rotatedVector = Quaternion.AngleAxis(angle, Vector3.forward) * originalVector;
        //Vector3 rotatedVector = Quaternion.AngleAxis(angle, Vector3.up) * originalVector;
        Debug.DrawArrow(Vector3.zero, rotatedVector.X0Y(), Color.red);
        Debug.DrawArrow(Vector3.zero, axis, Color.blue);
        */
    }

    void OnReady()
    {        
        //CompanionLink
        playerInput.OnClick += (pos, state) =>
        {
            if (state != ButtonState.Down) return;
            SetFormations(pos);
        };
        instantiator.RegisterPrefab(unitPrefab);
    }

    private void SetFormations(Vector2 position)
    {
        float2 pos = position;
        NativeArray<Unit> units = new NativeArray<Unit>(size, Allocator.TempJob);
        for (int i = 0; i < size; i++)
        {
            Entity entity = instantiator.Instantiate(unitPrefab);
            float2 size = new float2(1,1);
            units[i] = new Unit() 
            {
                entity = entity,
                transform = new UnitTransform() { size = size }
            };
        }

        //instantiator.Instantiate(unitPrefab, size);


        UnitGroupingJob unitGroupingJob = new UnitGroupingJob()
        {
            minGroupSize = 1,
            spacing = new float2(0.5f, 0.5f),
            groups = new UnitTransform() { size = new float2(10, 10), position = float2.zero },
            units = units
        };

        var jobHandle = unitGroupingJob.Schedule();
        jobHandle.Complete();

        //just for debugging
        for (int i = 0; i < size; i++)
        {
            Debug.Log(units[i].entity +" - "+ units[i].transform.position.X1Y());
            instantiator.Manager.SetName(units[i].entity, $"S {i}");
            instantiator.Manager.SetComponentData(units[i].entity, new Translation() { Value = (pos.X1Y() + units[i].transform.position.X1Y()) });

            //NonUniformScale scale = new NonUniformScale() { Value = units[i].transform.size.X1Y() };
            //instantiator.Manager.SetComponentData(units[i].entity, scale);
        }
        units.Dispose();
    }

    private void Update()
    {
    }
}
