using Gtion.Plugin.DI;
using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;
using Unity.Collections;
public partial class GiveOrderSystem : GtionSystemBase
{
    [GInject]
    PathFinder pathFinder;
    EndInitializationEntityCommandBufferSystem ecb;

    protected override void OnCreate()
    {
        base.OnCreate();
        ecb = this.World.GetOrCreateSystem<EndInitializationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate()
    {
        //EntityManager.GetSharedComponentData

        //ignore this system if no new order created
        //if (!pathFinder.IsNewOrderArrived) return;
        
        /*
        var commandBuffer = ecb.CreateCommandBuffer();
        Entities.WithAll<SelectedUnitTag>().ForEach((in Entity e) => 
        {
            commandBuffer.RemoveComponent<SelectedUnitTag>(e);
            //EntityManager.GetSharedComponentData<>

        });
        */

        /*
        var selectedTagType = ComponentType.ReadOnly<SelectedUnitTag>();
        EntityQuery entityQuery = GetEntityQuery(selectedTagType);
        EntityManager.RemoveComponent(entityQuery, selectedTagType);

        //UnitOrder unitOrder = new UnitOrder() { };
        //EntityManager.AddSharedComponentData(entityQuery, unitOrder);
        */
    }
}
