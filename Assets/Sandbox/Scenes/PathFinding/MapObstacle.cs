using NaughtyAttributes;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class MapObstacle : MonoBehaviour
{
    [SerializeField]
    bool drawObstacle;
    [SerializeField]
    Vector3[] obstaclePoint;

    [Button]
    void UpdateObstacle()
    {
        obstaclePoint = new Vector3[transform.childCount];
        for (int i = 0; i < obstaclePoint.Length; i++)
        {
            obstaclePoint[i] = transform.GetChild(i).localPosition;
        }
    }

    [Button]
    void AddObstacle()
    {
        var obj = new GameObject("ObstaclePoint");
        obj.transform.parent = transform;
        obj.transform.localPosition = Vector3.zero;
        UpdateObstacle();
    }

    void OnDrawGizmos()
    {
        if (!drawObstacle || obstaclePoint.Length < 2) return;

        Vector3 prev = obstaclePoint[obstaclePoint.Length - 1];
        for (int i = 0; i < obstaclePoint.Length; i++)
        {
            Debug.DrawLine(transform.position+ prev , transform.position + obstaclePoint[i], Color.red);
            prev = obstaclePoint[i];
        }
    }

    public ObstaclesLine[] GetObstaclesLines() 
    {
        ObstaclesLine[] result = new ObstaclesLine[obstaclePoint.Length];
        Vector3 prev = obstaclePoint[obstaclePoint.Length - 1];
        for (int i = 0; i < obstaclePoint.Length; i++)
        {
            Vector2 startPoint = (transform.position + prev).XZ(); 
            Vector2 endPoint = (transform.position + obstaclePoint[i]).XZ();
            result[i] = new ObstaclesLine() { startPoint = startPoint, endPoint = endPoint };

            prev = obstaclePoint[i];
        }
        return result;
    }
}
