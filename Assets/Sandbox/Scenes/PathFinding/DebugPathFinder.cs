using Gtion.Plugin.DI;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Unity.Entities;
using UnityEngine;

public class DebugPathFinder : MonoBehaviour
{
    [GInject]
    IPlayerInput playerInput;

    [GInject]
    PathFinder pathFinder;

    [GInject]
    EntityManager entityManager;

    [SerializeField]
    int orderCount;
    
    void Start()
    {
        GDi.Request(this, ()=> 
        {
            playerInput.OnClick += OnClick;
        });
    }

    private void Update()
    {
        orderCount= pathFinder.Orders.Count;
    }

    void OnClick(Vector2 position, ButtonState buttonState) 
    {
        if (buttonState == ButtonState.Down)
        {
            CreateOrder(position);
        }
    }

    void CreateOrder(Vector2 targetPosition)
    {
        Stopwatch stopWatch = new Stopwatch();
        stopWatch.Start();

        //int orderId = pathFinder.RequestOrder(targetPosition, 2);



        stopWatch.Stop();
        // Get the elapsed time as a TimeSpan value.
        System.TimeSpan ts = stopWatch.Elapsed;

        // Format and display the TimeSpan value.
        string elapsedTime = System.String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
            ts.Hours, ts.Minutes, ts.Seconds,
            ts.Milliseconds / 10);
        Debug.Log("RunTime " + stopWatch.Elapsed.Milliseconds);
    }

    void GenerateUnit() 
    {
        
    }
}
