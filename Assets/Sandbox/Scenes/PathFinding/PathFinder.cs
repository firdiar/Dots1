using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Unity.Mathematics;
using Gtion.Plugin.DI;
using System;
using Unity.Entities;
using Unity.Collections;

[System.Serializable]
public struct OrderGrids
{
    const float Middle = 0.7071067811865475f;
    public static readonly float2[] Directions = new float2[]
    {
        new float2(0 , 0),
        new float2(-Middle , Middle),   new float2(0 , 1), new float2(Middle , Middle),
        new float2(-1 , 0),             new float2(1 , 0),
        new float2(-Middle , -Middle),  new float2(0 , -1),new float2(Middle , -Middle)
    };

    public static readonly Dictionary<byte, Vector2Int> ByteToVector = new Dictionary<byte, Vector2Int>()
    {
        { 1 , new Vector2Int(-1 , 1) }, { 2 , new Vector2Int(0 , 1) }, { 3 , new Vector2Int(1 , 1) },
        { 4 , new Vector2Int(-1 , 0) }, { 0 , new Vector2Int(0 , 0) }, { 5 , new Vector2Int(1 , 0) },
        { 6 , new Vector2Int(-1 , -1) }, { 7 , new Vector2Int(0 , -1) }, { 8 , new Vector2Int(1 , -1) },
    };

    public static readonly Dictionary<Vector2Int, byte> VectorToByte = new Dictionary<Vector2Int, byte>()
    {
        { new Vector2Int(-1 , 1) , 1 }, { new Vector2Int(0 , 1)  , 2  }, { new Vector2Int(1 , 1) , 3  },
        { new Vector2Int(-1 , 0) , 4  }, {  new Vector2Int(0 , 0), 0 }, {  new Vector2Int(1 , 0) , 5 },
        { new Vector2Int(-1 , -1), 6 }, {  new Vector2Int(0 , -1), 7 }, {  new Vector2Int(1 , -1), 8},
    };

    [SerializeField]
    public bool isValid;
    [SerializeField]
    public byte[] movementOrder;
}

public struct orderCounter
{
    [SerializeField]
    public int orderCount;
    [SerializeField]
    public int finishedCount;
}

public class PathFinder : MonoBehaviour
{
    [SerializeField]
    bool enableGizmo = true;

    [RequireType(typeof(IMapGrids))]
    [SerializeField]
    UnityEngine.Object mapGrids;
    [GInject]
    IMapGrids InjectMapGrids;
    IMapGrids MapGrids => InjectMapGrids == null ? (mapGrids as IMapGrids) : InjectMapGrids;    


    [Header("Order")]
    [SerializeField]
    int orderStart;
    [SerializeField]
    int orderFinish;

    [SerializeField]
    [NaughtyAttributes.ReadOnly]
    int activeOrder;

    public int OrderStart
    {
        get { return orderStart; }
        private set
        {
            orderStart = value;
            activeOrder = orderFinish - orderStart;
        }
    }
    public int OrderFinish
    {
        get { return orderFinish; }
        private set {
            orderFinish = value;
            activeOrder = orderFinish - orderStart;
        }
    }

    //PR ini ga selamanya selesai berurutan
    Dictionary<int, OrderGrids> gridOrder = new Dictionary<int, OrderGrids>();
    Dictionary<int, orderCounter> counterOrder = new Dictionary<int, orderCounter>();

    public IReadOnlyDictionary<int, OrderGrids> Orders => gridOrder;
    public GridSettings Settings => MapGrids.Settings;

    #region Editor
    [Header("Editor Only")]
    [SerializeField]
    OrderGrids orderGrid;

    public void OnDrawGizmos()
    {
        if (!enableGizmo) return;

        MapGrids.Settings.DrawOrder(orderGrid.movementOrder);
    }
    #endregion

    private void Start()
    {
        GDi.Register(this);
        GDi.Request(this);
    }

    #region API
    public int RequestOrder(HashSet<int> startPoint, HashSet<int> finishPoint, int requester) 
    {
        if (requester == 0)
        {
            Debug.Log("Invalid Request");
            return -1;
        }

        OrderGrids orderGrid = GenerateOrderGrid(MapGrids, startPoint , finishPoint);
        if (orderGrid.isValid)
        {
            gridOrder.Add(OrderFinish, orderGrid);
            counterOrder.Add(orderFinish, new orderCounter() { finishedCount = 0, orderCount = requester });
#if UNITY_EDITOR
            this.orderGrid = orderGrid;//debugging
#endif
            int result = OrderFinish;
            OrderFinish++;
            return result;
        }
        else
        {
            return -1;
        }
    }
    public void FinishOrder(int orderId, int amount)
    {
        if (amount <= 0) return;

        if (counterOrder[orderId].finishedCount + amount < counterOrder[orderId].orderCount)
        {
            var temp = counterOrder[orderId];
            temp.finishedCount += amount;
            counterOrder[orderId] = temp;
            //Debug.Log($"Progressing Order {temp.finishedCount}/{temp.orderCount}");
        }
        else
        {
            OrderStart++;
            gridOrder.Remove(orderId);
            counterOrder.Remove(orderId);
            Debug.Log($"Removing Pathfinding Order");// {orderId}");
        }
    }
    #endregion


    #region Static
    struct NodeOrderHelper
    {
        public bool isRegistered;
        public bool isChecked;

        public float distance;
    }

    public static OrderGrids GenerateOrderGrid(IMapGrids map, HashSet<int> startingPoints, HashSet<int> finishPoints)
    {
        int totalGrid = map.Settings.TotalGrid;

        NodeOrderHelper[] nodes = new NodeOrderHelper[totalGrid];
        byte[] directions = new byte[totalGrid];
        //Array.Fill(directions, byte.MaxValue);//set all blocked

        foreach (var index in finishPoints)
        {
            if (!map.Settings.IsIndexInRange(index))
            {
                Debug.Log($"Unable to reach finish points, overflow : {index}");
                return new OrderGrids() { isValid = false };
            }

            nodes[index].isChecked = true;
            nodes[index].isRegistered = true;
            nodes[index].distance = 0;
            directions[index] = 128;

            if (startingPoints.Contains(index))
            {
                startingPoints.Remove(index);
            }
        }

        List<int> nextCheck = new List<int>(256); // 1 kb
        int[] currentCheck = finishPoints.ToArray();

        int totalLoop = 0;//avoid infiniteloop
        int loop = 0;
        while (loop < currentCheck.Length && startingPoints.Count > 0 && totalLoop < totalGrid)
        {            
            int nodeIndex = currentCheck[loop];
            loop++;
            totalLoop++;

            //no need to check blocked node and it's sorounding
            if (!map.Settings.IsIndexInRange(nodeIndex))
            {
                ContinueLoop(ref loop, ref currentCheck, nextCheck);
                continue;
            }

            if (startingPoints.Contains(nodeIndex))
            {
                startingPoints.Remove(nodeIndex);
            }

            //check sorounding grids
            SoroundingCheck(map, startingPoints, nodeIndex, nodes, nextCheck, directions);

            nodes[nodeIndex].isChecked = true;
            //Debug.Log($"Check {map.Settings.GetNodeGrid(nodeIndex)}");

            //prepare for next loop            
            ContinueLoop(ref loop, ref currentCheck, nextCheck);
        }

        if (startingPoints.Count > 0)
        {
            //debugging only
            Debug.Log($"Unable to get these points {totalLoop}/{totalGrid}#{startingPoints.Count}#{loop}/{currentCheck.Length}");
            /*
            foreach (var idx in startingPoints)
            {
                var posGrid = map.Settings.GetNodeGrid(idx);
                Debug.Log($" #{posGrid} Block : {map.WorldGridNode[idx].blocked} ");
                Debug.DrawCircle(map.Settings.GetGridPosition(posGrid.x, posGrid.y).X0Y(), 1, Color.red, duration: 5);
            }
            */
            return new OrderGrids() { isValid = false };
        }

        OrderGrids order = new OrderGrids()
        {
            isValid = true,
            movementOrder = directions,
        };
        return order;
    }

    static void ContinueLoop(ref int loop, ref int[] currentCheck , List<int> nextCheck) 
    {
        if (loop >= currentCheck.Length)
        {
            loop = 0;
            currentCheck = nextCheck.ToArray();
            nextCheck.Clear();
        }
    }

    static void SoroundingCheck(IMapGrids map, HashSet<int> startingPoints, int nodeIndex, NodeOrderHelper[] nodes, List<int> nextCheck, byte[] directions)
    {
        bool blockedNode = map.WorldGridNode[nodeIndex].blocked;
        float nodeSize = map.Settings.nodeSize;
        Vector2Int myGridPosition = map.Settings.GetNodeGrid(nodeIndex);
        for (int i = -1; i <= 1; i++)
        {
            for (int j = -1; j <= 1; j++)
            {
                if (i == 0 && j == 0) continue; //ignore self

                int x = myGridPosition.x + i;
                int y = myGridPosition.y + j;
                int soroundNodeIndex = map.Settings.GetNodeIndex(x, y);

                if (soroundNodeIndex == -1) continue;

                if (map.WorldGridNode[soroundNodeIndex].blocked)
                {
                    bool isStartingpoint = startingPoints.Contains(soroundNodeIndex);
                    if (isStartingpoint)
                    {
                        startingPoints.Remove(soroundNodeIndex);
                        directions[soroundNodeIndex] = OrderGrids.VectorToByte[new Vector2Int(-i, -j)]; ;
                    }
                    else
                    {
                        directions[soroundNodeIndex] = byte.MaxValue;
                    }
                    continue;//ignore blocked node and out of range
                }

                if (nodes[soroundNodeIndex].isChecked) //checking prev nodes
                {
                    float distance = new Vector2(i, j).magnitude;
                    float preNodeDist = nodes[soroundNodeIndex].distance + (nodeSize * distance);
                    if (!nodes[nodeIndex].isChecked || nodes[nodeIndex].distance > preNodeDist)
                    {
                        nodes[nodeIndex].isChecked = true;
                        nodes[nodeIndex].distance = preNodeDist;
                        directions[nodeIndex] = OrderGrids.VectorToByte[new Vector2Int(i, j)];
                    }
                }
                else if (!blockedNode && !nodes[soroundNodeIndex].isRegistered)
                {
                    nodes[soroundNodeIndex].isRegistered = true;
                    nextCheck.Add(soroundNodeIndex);
                }
            }
        }
    }
    #endregion
}
