using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

[System.Serializable]
public struct NodePoint
{
    public static NodePoint Null => new NodePoint() { index = -1 };
    public static bool IsNull(NodePoint point) => point.index == -1;

    public bool isInitialized;

    public int index;
    //public int directionToTarget;//index of another node

    //public float2 position; //basically we can get this from calculation
    public bool blocked;
}