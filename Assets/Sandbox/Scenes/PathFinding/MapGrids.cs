using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;
using System.Linq;
using Unity.Burst;
using Unity.Entities;
using System;
using NaughtyAttributes;
using Gtion.Plugin.DI;

[System.Serializable]
public struct ObstaclesLine
{
    public Vector2 startPoint;
    public Vector2 endPoint;
}

[System.Serializable]
public struct GridSettings
{
    public bool isInited;
    public float2 centerPoint;
    public float nodeSize;
    public Vector2Int gridSize;

    //auto
    [NaughtyAttributes.ReadOnly]
    [AllowNesting]
    public float2 worldSize;
    [NaughtyAttributes.ReadOnly]
    [AllowNesting]
    public float2 startPoint;
    [NaughtyAttributes.ReadOnly]
    [AllowNesting]
    public float2 endPoint;
    [NaughtyAttributes.ReadOnly]
    [AllowNesting]
    public float2 halfBlock;
    [NaughtyAttributes.ReadOnly]
    [AllowNesting]
    public int TotalGrid;
    //public int TotalGrid => gridSize.x * gridSize.y;
}

public interface IMapGrids
{
    public GridSettings Settings { get; }
    public NodePoint[] WorldGridNode { get; }

    public void ClearCache();
    public void Bake(ObstaclesLine[] obstaclesLine);
}

[System.Serializable]
public class MapGrids : MonoBehaviour, IMapGrids
{
    [SerializeField]
    [NaughtyAttributes.ReadOnly]
    bool isBaked;
    [SerializeField]
    GridSettings settings;
    [SerializeField]
    NodePoint[] worldGridNode;
    [SerializeField]
    MapObstacle[] obstacles;

    [Header("Debugging")]
    [SerializeField]
    bool enableGizmo;
    [SerializeField]
    bool showGrid;
    [SerializeField]
    bool isDebugMapGrids;

    public GridSettings Settings => settings;
    public NodePoint[] WorldGridNode => worldGridNode;

    private void Start()
    {
        if (isDebugMapGrids)
        {
            Destroy(this);
            return;
        }
        settings = settings.Init();
        if (!isBaked)
        {
            Bake();
        }

        GDi.Register<IMapGrids>(this);
    }

    [Button]
    void FindAllObstacles()
    {
        obstacles = FindObjectsOfType<MapObstacle>();
    }

    [Button]
    public void Bake()
    {
        settings = settings.Init();
        List<ObstaclesLine> obstaclesLines = new List<ObstaclesLine>();
        foreach (var obs in obstacles)
        {
            var arr = obs.GetObstaclesLines();
            obstaclesLines.AddRange(arr);
        }
        Bake(obstaclesLines.ToArray());
    }

    public void Bake(ObstaclesLine[] obstaclesLine)
    {
        worldGridNode = GenerateGridNode(Settings, obstaclesLine);
        isBaked = true;
    }

    [Button]
    public void ClearCache()
    {
        obstacles = new MapObstacle[0];
        worldGridNode = new NodePoint[0];
        isBaked = false;
    }

    public void OnDrawGizmos()
    {
        if (!enableGizmo) return;

        this.DrawWorld(!showGrid);
    }

    private void OnValidate()
    {
        settings = settings.Init();
    }

    #region Static
    public static NodePoint[] GenerateGridNode(GridSettings settings, ObstaclesLine[] obstacles)
    {
        if (!settings.isInited) settings = settings.Init();

        JobHandle handler = default;
        NativeArray<NodePoint> nodesJob = new NativeArray<NodePoint>(settings.gridSize.x * settings.gridSize.y, Allocator.TempJob);
        NativeArray<ObstaclesLine> obstaclesJob = new NativeArray<ObstaclesLine>(obstacles, Allocator.TempJob);

        //only execute job when there's obstacle
        if (obstacles != null && obstacles.Length > 0)
        {
            GenerateBlockJob generateBlockJob = new GenerateBlockJob()
            {
                settings = settings,
                nodes = nodesJob,
                obstacles = obstaclesJob
            };
            handler = generateBlockJob.Schedule(obstacles.Length, 5);
        }

        GenerateGridJob generateGridJob = new GenerateGridJob()
        {
            startPoint = settings.startPoint,
            halfBlock = settings.halfBlock,
            settings = settings,
            nodes = nodesJob
        };
        handler = generateGridJob.Schedule(nodesJob.Length, 64, handler);
        handler.Complete();

        NodePoint[] nodes = nodesJob.ToArray();
        nodesJob.Dispose();
        obstaclesJob.Dispose();

        return nodes;
    }
    #endregion

    [BurstCompile]
    struct GenerateBlockJob : IJobParallelFor
    {
        public GridSettings settings;

        [NativeDisableParallelForRestriction]
        public NativeArray<NodePoint> nodes;
        [Unity.Collections.ReadOnly]
        public NativeArray<ObstaclesLine> obstacles;

        public void Execute(int index)
        {
            NativeList<int> grids = new NativeList<int>(Allocator.Temp);
            settings.RaycastAll(obstacles[index].startPoint, obstacles[index].endPoint, grids);
            foreach (int gridIdx in grids)
            {
                BlockNode(gridIdx);
            }
            grids.Dispose();
        }

        void BlockNode(int index)
        {
            if (index >= 0 && index < nodes.Length)
            {
                nodes[index] = new NodePoint()
                {
                    isInitialized = true,
                    index = index,
                    blocked = true,
                };
            }
        }
    }

    [BurstCompile]
    struct GenerateGridJob : IJobParallelFor
    {
        public float2 startPoint;
        public float2 halfBlock;
        public GridSettings settings;
        public NativeArray<NodePoint> nodes;

        public void Execute(int index)
        {
            if (!nodes[index].isInitialized)
            {
                //var pos = settings.GetNodeGrid(index);
                nodes[index] = new NodePoint() {
                    isInitialized = true,
                    index = index,
                    blocked = false,
                };
            }
        }
    }    
}