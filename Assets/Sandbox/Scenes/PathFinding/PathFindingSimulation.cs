using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;
using NaughtyAttributes;
using Unity.Mathematics;
using Gtion.Plugin.DI;
using System.Diagnostics;



[ExecuteInEditMode]
public class PathFindingSimulation : MonoBehaviour
{
    [SerializeField]
    Vector2Int gridSize;
    [SerializeField]
    float nodeSize;

    [SerializeField]
    Vector2 centerPoint;

    [SerializeField]
    [ReadOnly]
    Vector2 worldSize;

    [SerializeField]
    Vector2 targetPosition;

    [SerializeField]
    float sighGradial;

    [SerializeField]
    NodePoint[] nodes;

    [SerializeField]
    MapObstacle[] obstacles;

    [Button]
    void Init()
    {
        nodes = GenerateGridNode(centerPoint , nodeSize, gridSize);
    }
    NodePoint[] GenerateGridNode(Vector2 centerPoint, float blockSize, Vector2Int gridSize)
    {
        var worldSize = nodeSize * new Vector2(gridSize.x, gridSize.y);
        float2 startPoint = centerPoint - (worldSize / 2);
        float2 halfBlock = new float2(blockSize, blockSize) * 0.5f;

        NodePoint[] nodes = new NodePoint[gridSize.x * gridSize.y];
        return nodes;
        /*
        foreach (var obs in obstacles)
        {
            obs.StepLoop(nodeSize, pos => nodes[GetNodeIndex(pos)].blocked = true);
        }

        for (int x = 0; x < this.gridSize.x; x++)
        {
            for (int y = 0; y < this.gridSize.y; y++)
            {
                int index = GetNodeIndex(x, y);
                if (nodes[index].blocked) continue;

                nodes[index] = new NodePoint() { index = index, directionToTarget = -1, position = (startPoint + new float2(blockSize * x, blockSize * y) + halfBlock) };
            }
        }

        return nodes;
        */
    }

    #region Debug
    [GInject]
    IPlayerInput input;

    private void Start()
    {
        GDi.Request(this, OnReady);
    }

    void OnReady()
    {
        input.OnClick += (input, state) =>
        {
            int index = GetNodeIndex(input);
            if (IsIndexInRange(index))
            {
                //nodes[index].blocked = true;
            }
        };
    }
    #endregion
    
    [SerializeField]
    Queue<int> queue = new Queue<int>();

    [Button]
    void GenerateHeatMap() 
    {
        Stopwatch stopWatch = new Stopwatch();
        stopWatch.Start();

        int startingPoint = GetNodeIndex(targetPosition);
        /*
        nodes[startingPoint].isChecked = true;
        nodes[startingPoint].distance = 0;
        nodes[startingPoint].directionToTarget = -1;
        */
        queue.Enqueue(startingPoint);
        while (queue.Count > 0)
        {
            GenerateHeatMapCheck();
        }

        stopWatch.Stop();
        // Get the elapsed time as a TimeSpan value.
        System.TimeSpan ts = stopWatch.Elapsed;

        // Format and display the TimeSpan value.
        string elapsedTime = System.String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
            ts.Hours, ts.Minutes, ts.Seconds,
            ts.Milliseconds / 10);
        Debug.Log("RunTime " + stopWatch.Elapsed.Milliseconds);
    }

    void GenerateHeatMapCheck()
    {
        /*
        int nodeIndex = queue.Dequeue();
        if (nodes[nodeIndex].blocked || nodes[nodeIndex].isInitialized) return;
        
        Vector2Int myGridPosition = GetNodeGrid(nodeIndex);
        for (int i = -1; i <= 1; i++)
        {
            for (int j = -1; j <= 1; j++)
            {
                if (i == 0 && j == 0) continue; //ignore self

                int x = myGridPosition.x + i;
                int y = myGridPosition.y + j;
                if (!IsIndexInRange(x, y)) continue;

                int loopNodeIndex = GetNodeIndex(x, y);
                if (nodes[loopNodeIndex].isChecked) //checking pre nodes
                {
                    Vector2 direction = new Vector2(i, j);
                    float preNodeDist = nodes[loopNodeIndex].distance + (nodeSize * direction.magnitude);
                    if (!nodes[nodeIndex].isInitialized || nodes[nodeIndex].distance > preNodeDist)
                    {
                        nodes[nodeIndex].isInitialized = true;
                        nodes[nodeIndex].directionToTarget = loopNodeIndex;
                        nodes[nodeIndex].distance = preNodeDist;
                    }
                }
                else //register next nodes
                {
                    queue.Enqueue(loopNodeIndex);
                }                
            }
        }

        nodes[nodeIndex].isChecked = true;
        */
    }

    #region Converter Method
    NodePoint GetNode(int x, int y)
    {
        int index = GetNodeIndex(x, y);
        if (index >= nodes.Length || index < 0)
        {
            return NodePoint.Null;
        }
        else
        {
            return nodes[index];
        }
    }
    int GetNodeIndex(int x, int y)
    {
        if (!IsIndexInRange(x,y)) return -1;

        return (y * gridSize.x) + x;
    }

    Vector2Int GetNodeGrid(int index)
    {
        int y = index / gridSize.x;
        int x = index - (y * gridSize.x);
        return new Vector2Int(x, y);
    }

    int GetDirectionIndex(int index, Vector2 direction)
    {
        int x = Mathf.Abs(direction.x) > 0.5f ? (int)Mathf.Sign(direction.x) : 0;
        int y = Mathf.Abs(direction.y) > 0.5f ? (int)Mathf.Sign(direction.y) : 0;

        Vector2Int myGrid = GetNodeGrid(index);
        Vector2Int targetGrid = new Vector2Int(myGrid.x + x, myGrid.y + y);
        return GetNodeIndex(targetGrid.x, targetGrid.y);
    }

    bool IsIndexInRange(int index)
    {
        return index >= 0 && index < nodes.Length;
    }

    bool IsIndexInRange(int x, int y)
    {
        return (x >= 0 && y >= 0 && x < gridSize.x && y < gridSize.y);
    }

    int GetNodeIndex(float2 worldPosition) 
    {
        float2 halfWorldSize = (nodeSize/2) * new Vector2(gridSize.x, gridSize.y);
        float2 startPoint = new float2(centerPoint) - halfWorldSize;
        float2 localPosition = (worldPosition - startPoint);

        int x = Mathf.FloorToInt( localPosition.x / nodeSize);
        int y = Mathf.FloorToInt( localPosition.y / nodeSize);
        return GetNodeIndex(x, y);
    }
    #endregion

    private void OnValidate()
    {
        worldSize = nodeSize * new Vector2(gridSize.x , gridSize.y);
    }
}
