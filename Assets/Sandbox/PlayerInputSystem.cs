using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;

public partial class PlayerInputSystem : SystemBase
{
    protected override void OnUpdate()
    {
        float deltaTime = Time.DeltaTime;
        Entities.ForEach((ref Translation translation ,in PadKeyComp input) =>
        {
            var speed = input.MovementSpeed * deltaTime;
            var deltaMovement = speed * input.GetYDirection();
            translation.Value.y += deltaMovement;
        }).Run();
    }
}
