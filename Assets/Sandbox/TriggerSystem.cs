using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;

[UpdateInGroup(typeof(FixedStepSimulationSystemGroup))]
//[UpdateAfter(typeof(StepPhysicsWorld))]
//[UpdateBefore(typeof(EndFramePhysicsSystem))]
public partial class TriggerSystem : SystemBase
{
    private EndSimulationEntityCommandBufferSystem endECBSystem;
    //private StepPhysicsWorld stepPhysicsWorld;

    protected override void OnStartRunning()
    {
        base.OnStartRunning();
        //this.RegisterPhysicsRuntimeSystemReadOnly();
    }


    protected override void OnCreate()
    {
        endECBSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
        //stepPhysicsWorld = World.GetOrCreateSystem<StepPhysicsWorld>();
    }

    protected override void OnUpdate()
    {
        //var triggerJob = new TriggerJob();
        //var collisionJob = new CollisionJob();

        //Dependency = triggerJob.Schedule(stepPhysicsWorld.Simulation, Dependency);
        //Dependency = collisionJob.Schedule(stepPhysicsWorld.Simulation, Dependency);
        //endECBSystem.AddJobHandleForProducer(Dependency);
    }
}
/*
[BurstCompile]
struct TriggerJob : ITriggerEventsJob
{
    public void Execute(TriggerEvent triggerEvent)
    {
        Debug.Log("Trigger");
    }
}

[BurstCompile]
struct CollisionJob : ICollisionEventsJob
{
    public void Execute(CollisionEvent triggerEvent)
    {
        Debug.Log("Collision");
    }
}
*/