using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridTest : MonoBehaviour
{
    [SerializeField]
    QuadrantGrid grid;

    private void OnDrawGizmos()
    {
        int index = grid.Settings.GetNodeIndex(transform.position.XZ());
        Debug.Log(grid.Settings.GetNodeGrid(index));
    }
}
