using Gtion.Plugin.DI;
using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

public class GameInitialize 
{
    [RuntimeInitializeOnLoadMethod]
    public static void Initialize()
    {
        //turn off physics
        Physics.autoSimulation = false;

        //target frame to Unlimited
        Application.targetFrameRate = -1;
        QualitySettings.vSyncCount = 0;//no sync

        GDi.Register(new MersenneTwister());

        Debug.Log("Game Settings Initialized!");
    }
}
