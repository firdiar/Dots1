using Gtion.Plugin.DI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Collections;

/// <summary>
/// Find my enemy here
/// </summary>
public interface IGamePlayers
{
    NativeArray<UnitOwner> GetMyEnemy(UnitOwner playerIdentity);
}
