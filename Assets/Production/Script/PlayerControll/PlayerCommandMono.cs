using Gtion.Plugin.DI;
using NaughtyAttributes;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;
using Unity.Collections;

public enum PlayerControlState
{ 
    Idle,
    SelectingUnit
}

public class PlayerCommandMono : MonoBehaviour
{
    [GInject]
    IPlayerInput playerInput;

    [SerializeField]
    [NaughtyAttributes.ReadOnly]
    PlayerControlState controlState;

    [SerializeField]
    Entity myCommandEntity;
    [SerializeField]
    UnitOwner myIdentity;

    [Header("Debugging")]
    [SerializeField]
    bool registerEnemy;

    bool isDragging = false;
    Vector2 selectionStartPoint;
    Vector2 selectionFinishPoint;

    World DefaultWorld => World.DefaultGameObjectInjectionWorld;
    EntityManager entityManager => DefaultWorld.EntityManager;

    RegisterCommandSystem CommandSystem => DefaultWorld.GetOrCreateSystem<RegisterCommandSystem>();

    #region RegisterPlayer
    void Start()
    {
        controlState = PlayerControlState.Idle;
        GDi.Request(this, OnReady);
    }

    void OnReady() 
    {
        RegisterNewPlayer();
        playerInput.OnClick += OnPointerClick;
        playerInput.OnCancelAction += OnCancelSelection;

        if (registerEnemy)
        {
            CommandSystem.RegisterNewPlayer(out _, out _);
        }
    }

    void RegisterNewPlayer() 
    {
        CommandSystem.RegisterNewPlayer(out myCommandEntity, out myIdentity);
    }
    #endregion

    void OnPointerClick(Vector2 position, ButtonState buttonState) 
    {
        if (buttonState == ButtonState.Down && controlState == PlayerControlState.SelectingUnit)
        {
            MoveUnits(position);
        }

        if (buttonState == ButtonState.Down && controlState == PlayerControlState.Idle)
        {
            isDragging = true;
            selectionStartPoint = position;
        }

        if (buttonState == ButtonState.Press && controlState == PlayerControlState.Idle)
        {
            selectionFinishPoint = position;
        }

        if (buttonState == ButtonState.Up && controlState == PlayerControlState.Idle)
        {
            isDragging = false;
            selectionFinishPoint = position;
            SelectUnits(selectionStartPoint, selectionFinishPoint);
        }
    }

    void OnCancelSelection()
    {
        CancelSelection();
    }

    private void OnDrawGizmos()
    {
        if (isDragging)
        {
            GetNormalizeRectSelection(in selectionStartPoint, in selectionFinishPoint, out var start, out var finish);
            Debug.DrawRectangleXZ(start, finish, Color.blue);
        }
    }

    void GetNormalizeRectSelection(in Vector2 originstart, in Vector2 originfinish, out Vector2 start, out Vector2 finish) 
    {
        float height = originfinish.y - originstart.y;
        float width = originfinish.x - originstart.x;

        start = originstart;
        finish = originstart;
        if (height < 0)
        {
            start.y += height;
        }
        else
        {
            finish.y += height;
        }

        if (width < 0)
        {
            start.x += width;
        }
        else
        {
            finish.x += width;
        }
    }

    #region API
    void SelectUnits(Vector2 startPoint, Vector2 finishPoint) 
    {
        GetNormalizeRectSelection(in startPoint, in finishPoint, out var start, out var finish);
        entityManager.AddComponentData(myCommandEntity, new SelectCommand()
        {
            selectStartPosition = start,
            selectEndPosition = finish
        });
        controlState = PlayerControlState.SelectingUnit;
        Debug.Log($"Command, {typeof(SelectCommand)} : {myCommandEntity.Index} - {myCommandEntity.Version}");
    }

    [Button]
    void CancelSelection()
    {
        entityManager.AddComponentData(myCommandEntity, new UnSelectCommand());
        controlState = PlayerControlState.Idle;

        Debug.Log($"Command, {typeof(UnSelectCommand)} : {myCommandEntity.Index} - {myCommandEntity.Version}");
    }

    void MoveUnits(Vector2 position)
    {
        var query = CommandSystem.GetSelectedUnit(myIdentity);
        if (query.CalculateEntityCount() == 0)
        {
            controlState = PlayerControlState.Idle;
            return;
        }

        entityManager.AddComponentData(myCommandEntity, new MoveCommand()
        {
            targetPosition = position
        });
        controlState = PlayerControlState.Idle;
        Debug.Log($"Command, {typeof(MoveCommand)} : {myCommandEntity.Index} - {myCommandEntity.Version}");
    }
    #endregion
}
