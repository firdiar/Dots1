using Gtion.Plugin.DI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ButtonState
{ 
    Down,
    Press,
    Up
}

public interface IPlayerInput
{
    public event Action<Vector2, ButtonState> OnClick;
    public event Action OnCancelAction;
}

public class PlayerInputHandler : MonoBehaviour, IPlayerInput
{
    public event Action<Vector2, ButtonState> OnClick;
    public event Action OnCancelAction;

    Camera mainCamera;
    Plane plane;
    Vector3 prevHitPoint;
    void Start()
    {
        // this creates a horizontal plane passing through this object's center
        plane = new Plane(Vector3.up, Vector3.zero);
        mainCamera = Camera.main;
        GDi.Register<IPlayerInput>(this);
    }
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ClickScreen(ButtonState.Down);
        }
        else if (Input.GetMouseButton(0))
        {
            ClickScreen(ButtonState.Press);
        }
        else if (Input.GetMouseButtonUp(0))
        {
            ClickScreen(ButtonState.Up);
        }

        if (Input.GetMouseButtonDown(1)) //right click
        {
            OnCancelAction?.Invoke();
        }
    }

    void ClickScreen(ButtonState state)
    {
        if (mainCamera.orthographic)
        {
            Vector2 worldPos = mainCamera.ScreenToWorldPoint(Input.mousePosition).XZ();
            OnClick?.Invoke(worldPos, state);
        }
        else
        {

            // create a ray from the mousePosition
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            // plane.Raycast returns the distance from the ray start to the hit point
            float distance;
            Vector3 hitPoint;
            if (plane.Raycast(ray, out distance))
            {
                // some point of the plane was hit - get its coordinates
                hitPoint = ray.GetPoint(distance);
                prevHitPoint = hitPoint;
            }
            else
            {
                hitPoint = prevHitPoint;
            }
            OnClick?.Invoke(hitPoint.XZ(), state);
        }
    }
}
