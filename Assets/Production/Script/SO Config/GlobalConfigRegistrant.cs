using Gtion.Plugin.DI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalConfigRegistrant : MonoBehaviour
{
    [SerializeField]
    UnitSizeGroupingConfig UnitSizeGroupingConfig;

    void Start()
    {
        GDi.Register(UnitSizeGroupingConfig);
    }
}
