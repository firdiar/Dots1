using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

[CreateAssetMenu( menuName = "SO/Config/UnitSize" , fileName = "UnitSizeConfig")]
public class UnitSizeGroupingConfig : ScriptableObject, IReadOnlyDictionary<UnitSizeType, float2>
{
    [SerializeField]
    GenericDictionary<UnitSizeType, float2> sizeGroup = new GenericDictionary<UnitSizeType, float2>();

    public float2 this[UnitSizeType key] => sizeGroup[key];

    public IEnumerable<UnitSizeType> Keys => sizeGroup.Keys;

    public IEnumerable<float2> Values => sizeGroup.Values;

    public int Count => sizeGroup.Count;

    public bool ContainsKey(UnitSizeType key)
    {
        return sizeGroup.ContainsKey(key);
    }

    public IEnumerator<KeyValuePair<UnitSizeType, float2>> GetEnumerator()
    {
        return sizeGroup.GetEnumerator();
    }

    public bool TryGetValue(UnitSizeType key, out float2 value)
    {
        return sizeGroup.TryGetValue(key, out value);
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return sizeGroup.GetEnumerator();
    }
}
