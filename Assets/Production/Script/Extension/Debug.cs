using System.IO;
using System.Runtime.CompilerServices;
using UnityEngine;

public class Debug : UnityEngine.Debug
{
    static private string GetIdentity(string path, int line) 
    {
        return $"{Path.GetFileName(path)}:{line}";
    }
    static private string GetMessage(object message, string path, int line) 
    {
        return $"[{GetIdentity(path, line)}]\n{message}";
    }

    static public new void Break ()
    {
        UnityEngine.Debug.Break();
    }

    static public void Log(object message, [CallerFilePath] string path = "", [CallerLineNumber] int line = 0)
    {
        UnityEngine.Debug.Log(GetMessage(message, path, line));
    }
 
    static public void Log (object message, Object context, [CallerFilePath] string path = "", [CallerLineNumber] int line = 0)
    {
        UnityEngine.Debug.Log(GetMessage(message, path, line), context);
    }
 
    static public void LogWarning (object message, [CallerFilePath] string path = "", [CallerLineNumber] int line = 0)
    {
        UnityEngine.Debug.LogWarning(GetMessage(message, path, line));
    }
 
    static public void LogWarning (object message, Object context, [CallerFilePath] string path = "", [CallerLineNumber] int line = 0)
    {
        UnityEngine.Debug.LogWarning(GetMessage(message, path, line), context);
    }
 
    static public void LogError (object message, [CallerFilePath] string path = "", [CallerLineNumber] int line = 0)
    {
        UnityEngine.Debug.LogError(GetMessage(message, path, line));
    }
 
    static public void LogError (object message, Object context, [CallerFilePath] string path = "", [CallerLineNumber] int line = 0)
    {
        UnityEngine.Debug.LogError(GetMessage(message, path, line), context);
    }

    #region Additional Debug
    public static void DrawCircle(Vector3 pos, float radius, Color color, int segments = 30, float duration = 0)
    {
        Debug.DrawEllipse(pos, Vector3.up, Vector3.forward, radius, radius, segments, color, duration);
    }
    public static void DrawEllipse(Vector3 pos, Vector3 forward, Vector3 up, float radiusX, float radiusY, int segments, Color color, float duration = 0)
    {
        float angle = 0f;
        Quaternion rot = Quaternion.LookRotation(forward, up);
        Vector3 lastPoint = Vector3.zero;
        Vector3 thisPoint = Vector3.zero;

        for (int i = 0; i < segments + 1; i++)
        {
            thisPoint.x = Mathf.Sin(Mathf.Deg2Rad * angle) * radiusX;
            thisPoint.y = Mathf.Cos(Mathf.Deg2Rad * angle) * radiusY;

            if (i > 0)
            {
                Debug.DrawLine(rot * lastPoint + pos, rot * thisPoint + pos, color, duration);
            }

            lastPoint = thisPoint;
            angle += 360f / segments;
        }
    }

    public static void DrawRectangleXZ(Vector2 start, Vector2 size, Color color)
    {
        float height = size.y - start.y;
        float width = size.x - start.x;

        DrawLine(start.X0Y(), (start + new Vector2(0 , height)).X0Y(), color);
        DrawLine(start.X0Y(), (start + new Vector2(width, 0)).X0Y(), color);

        DrawLine((start+ new Vector2(width, 0)).X0Y(), (start + new Vector2(width, height)).X0Y(), color);
        DrawLine((start+ new Vector2(0, height)).X0Y(), (start + new Vector2(width, height)).X0Y(), color);
    }

    public static void DrawRectangle(Vector3 position, Vector2 extent, Vector2 direction, Color color)
    {
        float angle = Vector2.SignedAngle(Vector2.up, direction);
        Vector3 rightVector = Quaternion.AngleAxis(angle, -Vector3.up) * Vector3.right;
        Vector3 forwardVector = Quaternion.AngleAxis(angle, -Vector3.up) * Vector3.forward;

        Debug.DrawArrow(position, direction.X0Y()/3, Color.blue);

        Vector3 rightOffset = rightVector * extent.x * 0.5f;
        Vector3 upOffset = forwardVector * extent.y * 0.5f;

        Vector3 offsetA = rightOffset + upOffset;
        Vector3 offsetB = -rightOffset + upOffset;
        Vector3 offsetC = -rightOffset - upOffset;
        Vector3 offsetD = rightOffset - upOffset;

        DrawLine(position + offsetA, position + offsetB, color);
        DrawLine(position + offsetB, position + offsetC, color);
        DrawLine(position + offsetC, position + offsetD, color);
        DrawLine(position + offsetD, position + offsetA, color);
    }

    public static void DrawRectangle(Vector3 position, Vector2 extent, Color color)
    {
        Vector3 rightOffset = Vector3.right * extent.x * 0.5f;
        Vector3 upOffset = Vector3.forward * extent.y * 0.5f;

        Vector3 offsetA = rightOffset + upOffset;
        Vector3 offsetB = -rightOffset + upOffset;
        Vector3 offsetC = -rightOffset - upOffset;
        Vector3 offsetD = rightOffset - upOffset;

        DrawLine(position + offsetA, position + offsetB, color);
        DrawLine(position + offsetB, position + offsetC, color);
        DrawLine(position + offsetC, position + offsetD, color);
        DrawLine(position + offsetD, position + offsetA, color);
    }

    public static void DrawArrow(Vector3 pos, Vector3 direction, Color color, float arrowHeadLength = 0.25f, float arrowHeadAngle = 20.0f)
    {
        Debug.DrawRay(pos, direction, color);

        Vector3 right = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 + arrowHeadAngle, 0) * new Vector3(0, 0, 1);
        Vector3 left = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 - arrowHeadAngle, 0) * new Vector3(0, 0, 1);
        Debug.DrawRay(pos + direction, right * arrowHeadLength, color);
        Debug.DrawRay(pos + direction, left * arrowHeadLength, color);
    }
    #endregion
}