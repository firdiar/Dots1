using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public static class VectorExtension
{
    public static readonly Quaternion Quaternion2DZ = Quaternion.Euler(90 , 0 , 0);

    public static Vector2 ToVector2(this float val) => new Vector2(val, val);
    public static Vector2 XZ(this Vector3 vector3)
    {
        return new Vector2(vector3.x, vector3.z);
    }
    public static Vector2 Right(this Vector2 dirr)
    {
        return new Vector2(dirr.y, -dirr.x);
    }

    public static Vector3 X1Y(this Vector2 vector2)
    {
        return new Vector3(vector2.x, 1, vector2.y);
    }

    public static Vector3 X0Y(this Vector2 vector2)
    {
        return new Vector3(vector2.x, 0, vector2.y);
    }

    public static Vector3 X1Y(this Vector2Int vector2)
    {
        return new Vector3(vector2.x, 1, vector2.y);
    }
    public static Vector3 X0Y(this Vector2Int vector2)
    {
        return new Vector3(vector2.x, 0, vector2.y);
    }

    public static Vector3 Right(this Vector3 dirr) 
    {
        return new Vector3(dirr.z, dirr.y, -dirr.x);
    }

    public static float2 XZ(this float3 vector3)
    {
        return new float2(vector3.x, vector3.z);
    }

    public static float3 X1Y(this float2 vector2)
    {
        return new Vector3(vector2.x, 1, vector2.y);
    }

    public static float3 X0Y(this float2 vector2)
    {
        return new Vector3(vector2.x, 0, vector2.y);
    }

    public static float LengthRect(this float2 vector2)
    {
        return math.abs(vector2.x) + math.abs(vector2.y);
    }
}
