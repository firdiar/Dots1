using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;

public static class MapGridExtension
{
    public const bool Debugging = false;

    public static GridSettings Init(this GridSettings settings)
    {
        settings.TotalGrid = settings.gridSize.x * settings.gridSize.y;
        settings.worldSize = settings.nodeSize * new Vector2(settings.gridSize.x, settings.gridSize.y);
        settings.startPoint = settings.centerPoint - (settings.worldSize / 2);
        settings.endPoint = settings.centerPoint + (settings.worldSize / 2);
        settings.halfBlock = new float2(settings.nodeSize, settings.nodeSize) * 0.5f;
        settings.isInited = true;

        //Debug.Log($"Settings Initied {settings.isInited}");
        return settings;
    }


    public static int GetNodeIndex(this GridSettings settings, int x, int y)
    {
        if (!IsIndexInRange(settings, x, y)) return -1;

        return (y * settings.gridSize.x) + x;
    }

    public static Vector2Int GetNodeGrid(this GridSettings settings, int index)
    {
        int y = index / settings.gridSize.x;
        int x = index - (y * settings.gridSize.x);
        return new Vector2Int(x, y);
    }

    public static Vector2Int GetNodeGrid(this GridSettings settings, float2 worldPosition)
    {
        float2 localPosition = (worldPosition - settings.startPoint);

        int x = Mathf.FloorToInt(localPosition.x / settings.nodeSize);
        int y = Mathf.FloorToInt(localPosition.y / settings.nodeSize);
        return new Vector2Int(x, y);
    }

    public static int GetDirectionIndex(this GridSettings settings, int index, Vector2 direction)
    {
        int x = Mathf.Abs(direction.x) > 0.5f ? (int)Mathf.Sign(direction.x) : 0;
        int y = Mathf.Abs(direction.y) > 0.5f ? (int)Mathf.Sign(direction.y) : 0;

        Vector2Int myGrid = settings.GetNodeGrid(index);
        Vector2Int targetGrid = new Vector2Int(myGrid.x + x, myGrid.y + y);
        return settings.GetNodeIndex(targetGrid.x, targetGrid.y);
    }

    public static Vector2 GetGridPosition(this GridSettings settings, int x , int y)
    {
        return (settings.startPoint + new float2(settings.nodeSize * x, settings.nodeSize * y) + settings.halfBlock);
    }

    public static float2 GetGridPosition(this GridSettings settings, int index)
    {
        var grid = settings.GetNodeGrid(index);
        return GetGridPosition(settings , grid.x , grid.y);
    }

    public static bool IsIndexInRange(this GridSettings settings, int index)
    {
        return index >= 0 && index < settings.TotalGrid;
    }

    public static bool IsIndexInRange(this GridSettings settings, int x, int y)
    {
        return (x >= 0 && y >= 0 && x < settings.gridSize.x && y < settings.gridSize.y);
    }

    public static int GetNodeIndex(this GridSettings settings, float2 worldPosition)
    {
        float2 localPosition = (worldPosition - settings.startPoint);

        int x = Mathf.FloorToInt(localPosition.x / settings.nodeSize);
        int y = Mathf.FloorToInt(localPosition.y / settings.nodeSize);
        return settings.GetNodeIndex(x, y);
    }

    public static void BoxcastEdge(this GridSettings settings, float2 center, float radius, out NativeParallelHashSet<int> gridIndexs, Allocator allocator)
    {
        float2 halfRadius = new float2(radius, radius);
        float2 start = center - halfRadius;
        float2 finish = center + halfRadius;
        BoxcastEdge(settings, start, finish, out gridIndexs, allocator);
    }

    public static void BoxcastEdge(this GridSettings settings, float2 startPosition, float2 finishPosition, out NativeParallelHashSet<int> gridIndexs, Allocator allocator)
    {
        float2 direction = finishPosition - startPosition;
        NativeList<int> temp = new NativeList<int>(Allocator.Temp);
        RaycastAll(settings, startPosition, startPosition + new float2(direction.x, 0), temp); // >
        RaycastAll(settings, startPosition, startPosition + new float2(0, direction.y), temp); // ^
        RaycastAll(settings, startPosition + new float2(0, direction.y), startPosition + new float2(direction.x, direction.y), temp); //_>
        RaycastAll(settings, startPosition + new float2(direction.x, 0), startPosition + new float2(direction.x, direction.y), temp);

        gridIndexs = new NativeParallelHashSet<int>(temp.Length, allocator);
        for(int i = 0; i< temp.Length; i++)
        {
            if (!gridIndexs.Contains(temp[i]))
            {
                gridIndexs.Add(temp[i]);
            }
        }
        temp.Dispose();
    }

    public static void BoxcastAll(this GridSettings settings, float2 startPosition, float2 finishPosition, out NativeParallelHashSet<int> gridIndexs, Allocator allocator)
    {
        float2 direction = finishPosition - startPosition;
        NativeList<int> temp = new NativeList<int>(Allocator.Temp);
        

        for (float i = 0; i < direction.y; i += settings.nodeSize)
        {
            RaycastAll(settings, startPosition + new float2(0, i), startPosition + new float2(direction.x, i), temp); // >
            //Debug.DrawLine((startPosition + new float2(0, i)).X0Y(), (startPosition + new float2(direction.x, i)).X0Y(), Color.blue, 5);
        }
        RaycastAll(settings, startPosition + new float2(0, direction.y), startPosition + direction, temp); // >
        //Debug.DrawLine((startPosition + new float2(0, direction.y)).X0Y(), (startPosition + direction).X0Y(), Color.blue, 5);

        gridIndexs = new NativeParallelHashSet<int>(temp.Length, allocator);
        for (int i = 0; i < temp.Length; i++)
        {
            if (!gridIndexs.Contains(temp[i]))
            {
                gridIndexs.Add(temp[i]);
                //Debug.DrawCircle(settings.GetGridPosition(temp[i]).X0Y() , 2, Color.cyan, duration: 5);
            }
        }
        temp.Dispose();
    }

    public static void RaycastAll(this GridSettings settings, float2 startPosition, float2 finishPosition, NativeList<int> gridIndexs)
    {
        float2 direction = finishPosition - startPosition;

        //Find Angle and Sign
        float xradAngle = Mathf.Deg2Rad * (90 - Vector2.Angle(Vector2.up, direction));
        float yradAngle = Mathf.Deg2Rad * (90 - Vector2.Angle(Vector2.right, direction));
        Vector2 selectionOffset = direction * -0.01f;// - math.normalize(direction) * (settings.nodeSize * 0.0001f);
        float2 directionSign = new Vector2(math.sign(direction.x), math.sign(direction.y));

        //Find Offset
        var gridPos = settings.GetNodeGrid(startPosition);
        float2 centerGrid = settings.GetGridPosition(gridPos.x , gridPos.y);
        float2 lengthOffset = (centerGrid - startPosition) + (directionSign * (settings.nodeSize / 2));
        lengthOffset = new Vector2(math.abs(lengthOffset.x), math.abs(lengthOffset.y));


        //basically below this one is Looping #1

        //Reserve for looping
        float xLength = lengthOffset.x;
        float yLength = lengthOffset.y;

        //make limit always positive and Sign will handle the actual direction
        float xLimit = Mathf.Abs(direction.x);
        float yLimit = Mathf.Abs(direction.y);
        
        //find first post for loop
        float2 xPos = GetPointFromDirectionX(xradAngle, xLength, (int)directionSign.x) + selectionOffset;
        float2 yPos = GetPointFromDirectionY(yradAngle, yLength, (int)directionSign.y) + selectionOffset;

        //calculate length of rect
        float lenX;// = xPos.LengthRect();
        float lenY;// = yPos.LengthRect();

        if (math.abs(xradAngle - (Mathf.PI / 2)) < 0.0001f && xPos.y < 0) //avoid infinite
        {
            lenX = float.MaxValue;
        }
        else
        {
            lenX = xPos.LengthRect();
        }

        if (math.abs(yradAngle - (Mathf.PI / 2)) < 0.0001f && yPos.x < 0) //avoid infinite
        {
            lenY = float.MaxValue;
        }
        else
        {
            lenY = yPos.LengthRect();
        }

        //avoid infinite loop
        int loopIdx = 0;
        while (xLength < xLimit || yLength < yLimit)
        {
            if (lenY < lenX) //cek
            {
                //do Y
                if (Debugging)
                {
#pragma warning disable CS0162 // Unreachable code detected
                    Debug.DrawArrow(startPosition.X0Y(), yPos.X0Y(), Color.green);
                    Debug.DrawRectangle((startPosition + yPos).X0Y(), Vector2.one, Color.green);
#pragma warning restore CS0162 // Unreachable code detected
                }

                int idx = settings.GetNodeIndex(startPosition + yPos);
                gridIndexs.Add(idx);

                yLength += settings.nodeSize;
                yPos = GetPointFromDirectionY(yradAngle, yLength, (int)directionSign.y) + selectionOffset; //maju                
                lenY = yPos.LengthRect();//math.length(yPos);                
            }
            else
            {
                //do X
                if (Debugging)
                {
#pragma warning disable CS0162 // Unreachable code detected
                    Debug.DrawArrow(startPosition.X0Y(), (xPos).X0Y(), Color.blue);
                    Debug.DrawRectangle((startPosition + xPos).X0Y(), Vector2.one, Color.blue);
#pragma warning restore CS0162 // Unreachable code detected
                }

                int idx = settings.GetNodeIndex(startPosition + xPos);
                gridIndexs.Add(idx);

                xLength += settings.nodeSize;
                xPos = GetPointFromDirectionX(xradAngle, xLength, (int)directionSign.x) + selectionOffset;
                lenX = xPos.LengthRect();//math.length(xPos);
            }

            loopIdx++;
            if (loopIdx >= 10000)
            {
                break;
            }
        }

        //finish        
        xPos = GetPointFromDirectionX(xradAngle, xLimit, (int)directionSign.x) + selectionOffset;
        int lastIndex = settings.GetNodeIndex(startPosition + xPos);
        gridIndexs.Add(lastIndex);
        
        if (Debugging)
        {
#pragma warning disable CS0162 // Unreachable code detected
            foreach (var gidx in gridIndexs)
            {
                Debug.DrawRectangle(settings.GetGridPosition(gidx).X0Y(), Vector2.one, Color.cyan);
            }
#pragma warning restore CS0162 // Unreachable code detected
        }
    }

    static Vector2 GetPointFromDirectionX(float radAngle , float xLength, int normalizer) 
    {
        float height = math.tan(radAngle) * xLength;
        return new Vector2(xLength * normalizer, height);
    }

    static Vector2 GetPointFromDirectionY(float radAngle, float yLength, int normalizer)
    {
        float width = math.tan(radAngle) * yLength;
        return new Vector2(width, yLength * normalizer);
    }

    public static void DrawWorld(this IMapGrids grids, bool boxOnly = false)
    {
        GridSettings settings = grids.Settings;
        if (!boxOnly)
        {
            int totalGrid = settings.TotalGrid;
            bool gridBaked = grids.WorldGridNode.Length == totalGrid;
            var size = Vector2.one * settings.nodeSize;
            for (int i = 0; i < totalGrid; i++)
            {
                var pos = settings.GetGridPosition(i).X0Y();
                Color col;
                if (gridBaked)
                {
                    col = grids.WorldGridNode[i].blocked ? Color.gray : Color.white;
                }
                else
                {
                    col = Color.white;
                }

                Debug.DrawRectangle(pos, size, col);
            }
        }

        Vector2 gridSize = settings.gridSize;
        Debug.DrawRectangle(settings.centerPoint.X0Y(), gridSize * settings.nodeSize, Color.green);
    }

    public static void DrawWorld(this GridSettings settings, bool boxOnly = false)
    {
        if (!boxOnly)
        {
            int totalGrid = settings.TotalGrid;
            var size = Vector2.one * settings.nodeSize;
            for (int i = 0; i < totalGrid; i++)
            {
                var pos = settings.GetGridPosition(i).X0Y();
                Debug.DrawRectangle(pos, size, Color.white);
            }
        }

        Vector2 gridSize = settings.gridSize;
        Debug.DrawRectangle(settings.centerPoint.X0Y(), gridSize * settings.nodeSize, Color.green);
    }

    public static void DrawOrder(this GridSettings settings, byte[] movementOrder)
    {
        var size = Vector2.one * settings.nodeSize;
        for (int i = 0; i < movementOrder.Length; i++)
        {
            var pos = settings.GetGridPosition(i).X0Y();// nodes[i].position.X1Y();
            
            if (movementOrder[i] == byte.MaxValue )//|| 
            {
                //Debug.DrawRectangle(pos, size, Color.gray);
                Debug.DrawArrow(pos, Vector3.forward, Color.red);
            }
            else if (movementOrder[i] != 0 && movementOrder[i] != 128 )
            {
                //Debug.DrawRectangle(pos, size, Color.white);
                Debug.DrawArrow(pos, OrderGrids.ByteToVector[movementOrder[i]].X0Y(), Color.blue);
            }
        }
    }

    public static int GetQuadrantIndex(this GridSettings settings, UnitOwner owner, float2 worldPos)
    {
        int index = GetNodeIndex(settings, worldPos);
        return GetQuadrantIndex(settings , owner , index);
    }

    public static int GetQuadrantIndex(this GridSettings settings, UnitOwner owner, int quadrantIndex)
    {
        int offset = settings.TotalGrid * owner.ownerId;
        return offset + quadrantIndex;
    }
}
