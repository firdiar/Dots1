using Gtion.Plugin.DI;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;

/// <summary>
/// Allow Instantiate Through Prefabs
/// </summary>
public class ECSInstantiate
{
    [RuntimeInitializeOnLoadMethod]
    private static void Initialize() => GDi.Register(new ECSInstantiate());

    EntityManager entityManager;
    GameObjectConversionSettings conversionSettings;

    Dictionary<GameObject, Entity> conversionCache = new Dictionary<GameObject, Entity>();

    bool isInitialized;

    public EntityManager Manager => entityManager;

    // Start is called before the first frame update
    public ECSInstantiate()
    {
        var defaultWorld = World.DefaultGameObjectInjectionWorld;
        entityManager = defaultWorld.EntityManager;
        ConvertToEntitySystem convertToEntitySystem = World.DefaultGameObjectInjectionWorld.GetExistingSystem<ConvertToEntitySystem>();
        conversionSettings = GameObjectConversionSettings.FromWorld(defaultWorld, convertToEntitySystem.BlobAssetStore);

        isInitialized = true;
    }

    public void RegisterPrefab(GameObject prefabs)
    {
        if (!isInitialized) return;

        Entity entityPrefab;
        entityPrefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(prefabs, conversionSettings);
        if (conversionCache.ContainsKey(prefabs))
        {
            conversionCache[prefabs] = entityPrefab;
        }
        else
        {
            conversionCache.Add(prefabs, entityPrefab);
        }
        entityManager.SetName(conversionCache[prefabs], $"{prefabs.name} (Clone)");
    }

    public Entity Instantiate(GameObject prefabs)
    {
        if (!isInitialized) return Entity.Null;

        Entity entityPrefab;
        if (!conversionCache.TryGetValue(prefabs, out entityPrefab))
        {
            RegisterPrefab(prefabs);
        }

        var instantiatedEntity = entityManager.Instantiate(entityPrefab);
        entityManager.SetName(instantiatedEntity, $"{prefabs.name} (Clone)");   

        return instantiatedEntity;
    }

    public NativeArray<Entity> Instantiate(GameObject prefabs, int count)
    {
        if (!isInitialized) return default;

        Entity entityPrefab;
        if (!conversionCache.TryGetValue(prefabs, out entityPrefab))
        {
            RegisterPrefab(prefabs);
        }

        var instantiatedEntity = entityManager.Instantiate(entityPrefab, count , Allocator.Temp);
        return instantiatedEntity;
    }
}
