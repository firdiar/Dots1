using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

public class ReflectionsHelper
{
    /// <summary>
    /// Get field using reflections
    /// </summary>
    /// <param name="target"></param>
    /// <param name="fieldName"></param>
    /// <returns></returns>
    public static MethodInfo GetMethod(object target, string fieldName)
    {
        if (target == null)
        {
            throw new ArgumentNullException("target", "The assignment target cannot be null.");
        }

        if (string.IsNullOrEmpty(fieldName))
        {
            throw new ArgumentException("fieldName", "The field name cannot be null or empty.");
        }

        Type t = target.GetType();
        MethodInfo mi = null;

        while (t != null)
        {
            var mis = t.GetMethods(BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
            mi = mis.FirstOrDefault(item => item.Name == fieldName && item.GetParameters().Length == 0);

            if (mi != null) break;

            t = t.BaseType;
        }

        if (mi == null)
        {
            throw new Exception(string.Format("Method '{0}' not found in type hierarchy.", fieldName));
        }

        return mi;
    }

    /// <summary>
    /// Set field using refelctions
    /// </summary>
    /// <param name="target"></param>
    /// <param name="fieldName"></param>
    /// <param name="value"></param>
    public static void SetField(object target, string fieldName, object value)
    {
        if (target == null)
        {
            throw new ArgumentNullException("target", "The assignment target cannot be null.");
        }

        if (string.IsNullOrEmpty(fieldName))
        {
            throw new ArgumentException("fieldName", "The field name cannot be null or empty.");
        }

        Type t = target.GetType();
        FieldInfo fi = null;

        while (t != null)
        {
            fi = t.GetField(fieldName, BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);

            if (fi != null) break;

            t = t.BaseType;
        }

        if (fi == null)
        {
            throw new Exception(string.Format("Field '{0}' not found in type hierarchy.", fieldName));
        }

        fi.SetValue(target, value);
        Debug.Log($"Value has been set. {t}");
    }

    // <summary>
    /// Get field using reflections
    /// </summary>
    /// <param name="target"></param>
    /// <param name="fieldName"></param>
    /// <returns></returns>
    public static object GetField(object target, string fieldName)
    {
        if (target == null)
        {
            throw new ArgumentNullException("target", "The assignment target cannot be null.");
        }

        if (string.IsNullOrEmpty(fieldName))
        {
            throw new ArgumentException("fieldName", "The field name cannot be null or empty.");
        }

        Type t = target.GetType();
        FieldInfo fi = null;

        while (t != null)
        {
            fi = t.GetField(fieldName, BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);

            if (fi != null) break;

            t = t.BaseType;
        }

        if (fi == null)
        {
            throw new Exception(string.Format("Field '{0}' not found in type hierarchy.", fieldName));
        }

        Debug.Log($"Value received. {t}");
        return fi.GetValue(target);
    }
}
