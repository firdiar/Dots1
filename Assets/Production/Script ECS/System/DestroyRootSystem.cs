using System.Collections;
using System.Collections.Generic;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

public struct DestroyHierarchyTag : IComponentData { }

public partial class DestroyRootSystem : GtionSystemBase
{
    EndSimulationEntityCommandBufferSystem m_endSimulationCmdBuffer;
    EntityQuery m_query;

    protected override void OnCreate()
    {
        base.OnCreate();
        m_endSimulationCmdBuffer = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
        m_query = GetEntityQuery(ComponentType.ReadOnly<DestroyHierarchyTag>() );
    }

    protected override void OnUpdate()
    {
        Dependency = new DestroyHierarchyJob
        {
            CmdBuffer = m_endSimulationCmdBuffer.CreateCommandBuffer().AsParallelWriter(),
            ChildrenFromEntity = GetBufferFromEntity<Child>(true)
        }.Schedule(m_query, Dependency);

        m_endSimulationCmdBuffer.CreateCommandBuffer().RemoveComponentForEntityQuery<DestroyHierarchyTag>(m_query);
        m_endSimulationCmdBuffer.AddJobHandleForProducer(Dependency);
    }

    [BurstCompile]
    partial struct DestroyHierarchyJob : IJobEntity
    {

        public EntityCommandBuffer.ParallelWriter CmdBuffer;
        [ReadOnly]
        public BufferFromEntity<Child> ChildrenFromEntity;

        public void Execute(Entity entity, [EntityInQueryIndex] int sortKey)
        {
            DestroyHierarchy(CmdBuffer, entity, sortKey, ChildrenFromEntity);
        }

        void DestroyHierarchy(EntityCommandBuffer.ParallelWriter cmdBuffer, Entity entity, int index, BufferFromEntity<Child> childrenFromEntity)
        {
            cmdBuffer.DestroyEntity(index, entity);
            if (!childrenFromEntity.HasComponent(entity))
                return;

            var children = childrenFromEntity[entity];
            for (var i = 0; i < children.Length; ++i)
            {
                var childEntity = children[i].Value;                
                DestroyHierarchy(cmdBuffer, childEntity, index, childrenFromEntity);
            }
        }
    }
}
