using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Collections;
using Unity.Burst;
using Unity.Transforms;
using Gtion.Plugin.DI;
using Unity.Jobs;
using Unity.Mathematics;

public struct UnitQuadrantGroup
{
    public Entity entity;
    public float2 position;
}

[AlwaysUpdateSystem]
public partial class QuadrantGroupSystem : GtionSystemBase
{
    
    public NativeParallelMultiHashMap<int, UnitQuadrantGroup> quadrantGroup;
    public NativeArray<NodePoint> mapWorld;

    EntityQuery quadrantUnitQuery;//only for units

    [GInject]
    IMapGrids mapGrids;
    [GInject]
    IQuadrantGrids quadrantGrids;

    List<UnitOwner> allCommander;
    protected override void OnCreate()
    {
        quadrantUnitQuery = GetEntityQuery(ComponentType.ReadOnly<UsingQuadrantTag>(), ComponentType.ReadOnly<UnitTag>(), ComponentType.ReadOnly<UnitOwner>());
        Debug.Log("Allocate HashMap");
        
        allCommander = new List<UnitOwner>(2);

        quadrantGroup = new NativeParallelMultiHashMap<int, UnitQuadrantGroup>(0, Allocator.Persistent);
        base.OnCreate();
    }

    protected override void OnDepedencyReady()
    {
        base.OnDepedencyReady();
        mapWorld = new NativeArray<NodePoint>(mapGrids.WorldGridNode, Allocator.Persistent);
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        Debug.Log("Deallocate HashMap");
        quadrantGroup.Dispose();
        mapWorld.Dispose();
    }

    protected override void OnUpdate()
    {
        allCommander.Clear();
        quadrantGroup.Clear();
        if (!quadrantGroup.IsCreated)
        {
            Debug.Log("Recreate HashMap");
            quadrantGroup = new NativeParallelMultiHashMap<int, UnitQuadrantGroup>(0, Allocator.Persistent);
        }

        EntityManager.GetAllUniqueSharedComponentData(allCommander);
        var nativeAllCommmander = allCommander.ToNativeArray(Allocator.Temp);

        //quadrantUnitQuery.ResetFilter();
        int entityCount = quadrantUnitQuery.CalculateEntityCountWithoutFiltering();
        if (entityCount > quadrantGroup.Capacity)
        {
            quadrantGroup.Capacity = entityCount; //resize
        }

        foreach (var commander in nativeAllCommmander)
        {
            quadrantUnitQuery.SetSharedComponentFilter(commander);
            AssignUnitToQuadrantJob assignJob = new AssignUnitToQuadrantJob()
            {
                unitOwner = commander,
                settings = quadrantGrids.Settings,
                quadrant = quadrantGroup.AsParallelWriter()
            };
            Dependency = assignJob.ScheduleParallel(quadrantUnitQuery, Dependency);
            Dependency.Complete();
        }        
        nativeAllCommmander.Dispose();
        //Debug.Log(quadrantGroup.Capacity);
        /*debug
        foreach (var pair in quadrantGroup)
        {
            foreach (var o in pair.Value)
            {
                Debug.Log(o.Key +" - "+o.Value.Index);
            }
        }
        */
    }

    [BurstCompile]
    public partial struct AssignUnitToQuadrantJob : IJobEntity
    {
        public UnitOwner unitOwner;
        public GridSettings settings;
        [WriteOnly]
        public NativeParallelMultiHashMap<int, UnitQuadrantGroup>.ParallelWriter quadrant;

        public void Execute(in Entity entity, in Translation translation) 
        {
            float2 position = translation.Value.XZ();
            int index = settings.GetQuadrantIndex(unitOwner, position);
            quadrant.Add(index, new UnitQuadrantGroup() { entity = entity, position = position });
        }
    }
}
