using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Gtion.Plugin.DI;
using Unity.Transforms;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using System;

public struct HasTarget : IComponentData
{
    public Entity entity;
}

[UpdateAfter(typeof(QuadrantGroupSystem))]
public partial class FindTargetSystem : GtionSystemBase
{
   // [GInject]
    QuadrantGroupSystem quadrantGroupSystem;

    [GInject]
    IQuadrantGrids quadrantGrids;

    List<UnitOwner> allCommander;
    EntityQuery findTargetQuery;
    EndInitializationEntityCommandBufferSystem ecb;

    protected override void OnCreate()
    {
        base.OnCreate();
        ecb = World.GetOrCreateSystem<EndInitializationEntityCommandBufferSystem>();
        findTargetQuery = GetEntityQuery(new EntityQueryDesc
        {
            All = new[] { ComponentType.ReadOnly<UnitTag>() , ComponentType.ReadOnly<UnitOwner>(), ComponentType.ReadOnly<UnitAttackRadius>(), },
            None = new[] { ComponentType.ReadOnly<UnitMovementOrder>(), ComponentType.ReadOnly<HasTarget>() }
        });
        allCommander = new List<UnitOwner>(2);
    }

    protected override void OnStartRunning()
    {
        base.OnStartRunning();
        quadrantGroupSystem = World.GetOrCreateSystem<QuadrantGroupSystem>();
    }

    public unsafe struct RadiusScanHistory 
    {
        public const int HistorySize = 8;

        public float2 scanPosition;
        public float radiusRange;
        public fixed int ScanHistory[HistorySize];
    }

    RadiusScanHistory GetRadiusScan(GridSettings settings, float2 position, float radius)
    {
        settings.BoxcastEdge(position, radius, out var gridIndexs, Allocator.Temp);
        RadiusScanHistory radiusScan = new RadiusScanHistory()
        {
            scanPosition = position,
            radiusRange = radius
        };

        int loop = 0;
        unsafe
        {
            foreach (int grid in gridIndexs)
            {
                radiusScan.ScanHistory[loop] = grid;
                loop++;
            }

            for (int i = loop; i < 8; i++)
            {
                radiusScan.ScanHistory[i] = -1;
            }
        }
        gridIndexs.Dispose();

        return radiusScan;
    }



    protected override void OnUpdate()
    {
        var quadrantGroup = quadrantGroupSystem.quadrantGroup;
        if (!quadrantGroup.IsCreated || quadrantGroup.IsEmpty)
        {
            Debug.Log($"Quadrant Group haven't initialized {quadrantGroup.IsCreated}");
            return;
        }

        var commandBuffer = ecb.CreateCommandBuffer();
        var cbParallel = commandBuffer.AsParallelWriter();
        var settings = quadrantGrids.Settings;

        allCommander.Clear();
        EntityManager.GetAllUniqueSharedComponentData<UnitOwner>(allCommander);
        var nativeAllCommmander = allCommander.ToNativeArray(Allocator.TempJob);
        for (int i = 0; i < nativeAllCommmander.Length; i++)
        {
            JobHandle job = Entities
                .WithStoreEntityQueryInField(ref findTargetQuery)                
                .WithSharedComponentFilter(nativeAllCommmander[i])
                .WithNone<HasTarget>()
                .WithReadOnly(nativeAllCommmander)
                .WithReadOnly(quadrantGroup)
                .WithBurst()
                .ForEach((Entity entity, int entityInQueryIndex, in Translation translation, in UnitAttackRadius attackRange) =>
            {
                //for (int i = 0; i < RadiusScanHistory.HistorySize; i++)
                float2 position = translation.Value.XZ();
                settings.BoxcastEdge(position, attackRange.detection, out var gridIndexs, Allocator.Temp);

                foreach(var qIndex in gridIndexs)
                {
                    //int qIndex = 0;// radiusHistory[i];
                    if (qIndex == -1) break; //basically if we reach -1, all next array should be -1

                    for (int j = 0; j < nativeAllCommmander.Length; j++)
                    {
                        if (i == j) continue;//loop for other commander,  note all other commander are enemies

                        int enemyQuadrantIndex = settings.GetQuadrantIndex(nativeAllCommmander[j], qIndex);
                        if (quadrantGroup.TryGetFirstValue(enemyQuadrantIndex, out var enemyUnit, out var iterator2))
                        {
                            do
                            {
                                if ((enemyUnit.position - position).LengthRect() < attackRange.detection)
                                {
                                    cbParallel.AddComponent(entityInQueryIndex, entity, new HasTarget() { entity = enemyUnit.entity });
                                }
                            } while (quadrantGroup.TryGetNextValue(out enemyUnit, ref iterator2));
                        }
                    }
                }        
            }).ScheduleParallel(Dependency);
            job.Complete();
        }
        nativeAllCommmander.Dispose();
    }
}
