using Gtion.Plugin.DI;
using System.Collections;
using System.Collections.Generic;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;



[UpdateInGroup(typeof(CommandGroup))]
public partial class RegisterMoveCommandSystem : GtionSystemBase
{
    [GInject]
    PathFinder pathFinder;

    EndSimulationEntityCommandBufferSystem ecb;
    EntityQuery commanderQuery;
    EntityQuery unitQuery;
    EntityQuery idleUnitQuery;
    EntityQuery movingUnitQuery;

    List<UnitPathFinding> existingOrders;
    protected override void OnCreate()
    {
        base.OnCreate();
        var commanderDesc = new EntityQueryDesc
        {
            All = new[] { ComponentType.ReadOnly<UnitOwner>(), ComponentType.ReadOnly<CommanderTag>(), ComponentType.ReadOnly<MoveCommand>() },
            None = new[] { ComponentType.ReadOnly<UnitTag>() }
        };
        commanderQuery = GetEntityQuery(commanderDesc);
        unitQuery = GetEntityQuery(ComponentType.ReadOnly<UnitTag>(), ComponentType.ReadOnly<SelectedUnitTag>(), ComponentType.ReadOnly<UnitOwner>());

        idleUnitQuery = GetEntityQuery(
                        new EntityQueryDesc
                        {
                            All = new[] { ComponentType.ReadOnly<UnitTag>(), ComponentType.ReadOnly<SelectedUnitTag>(), ComponentType.ReadOnly<UnitOwner>() },
                            None = new[] { ComponentType.ReadOnly<UnitPathFinding>() }
                        });

        movingUnitQuery = GetEntityQuery(
                new EntityQueryDesc
                {
                    All = new[] { ComponentType.ReadOnly<UnitPathFinding>(), ComponentType.ReadOnly<UnitTag>(), ComponentType.ReadOnly<SelectedUnitTag>(), ComponentType.ReadOnly<UnitOwner>() }
                });

        ecb = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
        existingOrders = new List<UnitPathFinding>();
    }

    protected override void OnUpdate()
    {
        if (commanderQuery.CalculateEntityCount() == 0) return;


        var commandBuffer = ecb.CreateCommandBuffer();
        var commander = commanderQuery.ToEntityArray(Allocator.Temp);
        for (int i = 0; i < commander.Length; i++)
        {
            UnitOwner ownerData = EntityManager.GetSharedComponentData<UnitOwner>(commander[i]);
            MoveCommand commandData = EntityManager.GetComponentData<MoveCommand>(commander[i]);
            UnitPathFinding pathFinding = GeneratePathFinding(commandBuffer, ownerData, commandData.targetPosition);// new UnitPathFinding();// { orderId = commandData.moveOrderId };

            if (pathFinding.orderId < 0) //path finding invalid
            {
                Debug.Log("Pathfinding Invalid");
                unitQuery.ResetFilter();
                unitQuery.SetSharedComponentFilter(ownerData);
                commandBuffer.RemoveComponentForEntityQuery<SelectedUnitTag>(unitQuery);
                return;
            }


            idleUnitQuery.SetSharedComponentFilter(ownerData);
            AddMoveCommand(commandBuffer, idleUnitQuery, pathFinding);


            existingOrders.Clear();
            EntityManager.GetAllUniqueSharedComponentData(existingOrders);
            for (int j = 0; j < existingOrders.Count; j++)
            {
                movingUnitQuery.ResetFilter();
                var existingOrder = existingOrders[j];
                movingUnitQuery.SetSharedComponentFilter(ownerData);
                movingUnitQuery.SetSharedComponentFilter(existingOrder);

                int unitCount = movingUnitQuery.CalculateEntityCount();
                pathFinder.FinishOrder(existingOrder.orderId, unitCount);

                commandBuffer.RemoveComponentForEntityQuery<UnitPathFinding>(movingUnitQuery);
                AddMoveCommand(commandBuffer, movingUnitQuery, pathFinding);
            }
        }
        commandBuffer.RemoveComponentForEntityQuery<MoveCommand>(commanderQuery);
    }

    void AddMoveCommand(EntityCommandBuffer commandBuffer, EntityQuery units, UnitPathFinding pathFinding)
    {
        commandBuffer.RemoveComponentForEntityQuery<SelectedUnitTag>(units);
        commandBuffer.AddSharedComponentForEntityQuery(units, pathFinding);
    }

    UnitPathFinding GeneratePathFinding(EntityCommandBuffer commandBuffer, UnitOwner owner, float2 targetPosition)
    {
        unitQuery.SetSharedComponentFilter(owner);
        int entityCount = unitQuery.CalculateEntityCount();

        GridSettings setting = pathFinder.Settings;
        float2 averagePosition = float2.zero;
        HashSet<int> startPoint = new HashSet<int>();
        UnitSizeType[] unitSizeTypes = new UnitSizeType[entityCount];

        NativeArray<SortHelper> entities = new NativeArray<SortHelper>(entityCount, Allocator.Temp);
        Entities            
            .WithSharedComponentFilter(owner)
            .WithAll<UnitTag>()
            .WithAll<SelectedUnitTag>()
            .ForEach((Entity entity, int entityInQueryIndex, in Translation translation, in UnitSize unitSize) =>
            {
                unitSizeTypes[entityInQueryIndex] = EntityManager.GetSharedComponentData<UnitSize>(entity).type;
                float2 position = translation.Value.XZ();
                averagePosition += position;
                int gridIndex = setting.GetNodeIndex(position);
                if (!startPoint.Contains(gridIndex))
                {
                    startPoint.Add(gridIndex);
                }

                entities[entityInQueryIndex] = new SortHelper() { entity = entity, position = position };
            }).WithoutBurst().Run();
        averagePosition /= entityCount;


        float2 direction = math.normalize(targetPosition - averagePosition);
        ArrangementResult result = UnitPlacementHandler.ArrangePlacement(setting, unitSizeTypes, targetPosition, direction);
        int orderId = pathFinder.RequestOrder(startPoint, result.finishPoint, entityCount);

        if (orderId >= 0)
        {
            if (entityCount < 1000)
            {
                entities.Sort(new EntitySort());
            }

            quaternion rotation = quaternion.LookRotation(new float3(direction.x, 0, direction.y), new float3(0, 1, 0));
            for (int i = 0; i < entities.Length; i++)
            {
                commandBuffer.AddComponent(entities[i].entity, new UnitMovementOrder() { targetPosition = result.placement[i], targetRotation = rotation });
            }
        }
        entities.Dispose();

        return new UnitPathFinding() { orderId = orderId };
    }

    #region Sort Entity
    public struct SortHelper
    {
        public Entity entity;
        public float2 position;
    }
    public struct EntitySort : IComparer<SortHelper>
    {
        public int Compare(SortHelper a, SortHelper b)
        {
            return a.entity.Index - b.entity.Index;
        }
    }
    #endregion
}
