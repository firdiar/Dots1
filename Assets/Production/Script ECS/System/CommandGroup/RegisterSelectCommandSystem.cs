using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;
using Unity.Collections;
using Gtion.Plugin.DI;
using Unity.Transforms;
using Unity.Jobs;
using Unity.Burst;

[UpdateInGroup(typeof(CommandGroup))]
public partial class RegisterSelectCommandSystem : GtionSystemBase
{
    [GInject]
    IQuadrantGrids quadrantGrids;

    QuadrantGroupSystem quadrantGroupSystem;
    EndSimulationEntityCommandBufferSystem ecb;
    EntityQuery commanderQuery;
    protected override void OnCreate()
    {
        base.OnCreate();
        var commanderDesc = new EntityQueryDesc
        {
            All = new[] { ComponentType.ReadOnly<UnitOwner>(), ComponentType.ReadOnly<CommanderTag>(), ComponentType.ReadOnly<SelectCommand>() },
            None = new[] { ComponentType.ReadOnly<UnitTag>() }
        };
        commanderQuery = GetEntityQuery(commanderDesc);
        ecb = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
        quadrantGroupSystem = World.GetOrCreateSystem<QuadrantGroupSystem>();
    }
    protected override void OnUpdate()
    {
        if (commanderQuery.CalculateEntityCount() == 0) return;


        var commander = commanderQuery.ToEntityArray(Allocator.Temp);
        
        var settings = quadrantGrids.Settings;
        var quadrantGroup = quadrantGroupSystem.quadrantGroup;
        for (int i = 0; i < commander.Length; i++)
        {
            UnitOwner ownerData = EntityManager.GetSharedComponentData<UnitOwner>(commander[i]);
            SelectCommand commandData = EntityManager.GetComponentData<SelectCommand>(commander[i]);

            settings.BoxcastAll(commandData.selectStartPosition, commandData.selectEndPosition, out var gridIndex, Allocator.TempJob);

            NativeList<JobHandle> jobs = new NativeList<JobHandle>(gridIndex.Count(), Allocator.Temp);
            foreach (var grids in gridIndex)
            {
                int quadrantIndex = settings.GetQuadrantIndex(ownerData , grids);
                if (quadrantGroup.TryGetFirstValue(quadrantIndex, out var item, out var it))
                {
                    var commandBuffer = ecb.CreateCommandBuffer();
                    SelectUnitJob job = new SelectUnitJob()
                    {
                        commandData = commandData,
                        parallelEcb = commandBuffer,
                        item = item,
                        it = it,
                        quadrantGroup = quadrantGroup
                    };
                    jobs.Add(job.Schedule(Dependency));
                }
            }
            JobHandle.CompleteAll(jobs);
            jobs.Dispose();
            gridIndex.Dispose();            
        }
        commander.Dispose();
        ecb.CreateCommandBuffer().RemoveComponentForEntityQuery<SelectCommand>(commanderQuery);
    }

    [BurstCompile]
    public struct SelectUnitJob : IJob
    {
        public SelectCommand commandData;
        public EntityCommandBuffer parallelEcb;

        public UnitQuadrantGroup item;
        [ReadOnly]
        public NativeParallelMultiHashMapIterator<int> it;
        [ReadOnly]
        public NativeParallelMultiHashMap<int, UnitQuadrantGroup> quadrantGroup;

        public void Execute()
        {
            int queryIndex = 0;
            do
            {
                bool isSelected =  //checking if unit inside rectangle selected
                        commandData.selectStartPosition.x <= item.position.x &&
                        commandData.selectStartPosition.y <= item.position.y &&
                        commandData.selectEndPosition.x >= item.position.x &&
                        commandData.selectEndPosition.y >= item.position.y;

                if (isSelected)
                {
                    parallelEcb.AddComponent<SelectedUnitTag>(item.entity);
                }
                queryIndex++;
            } while (quadrantGroup.TryGetNextValue(out item, ref it));
        }
    }
}
