using System.Collections;
using System.Collections.Generic;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;

[AlwaysUpdateSystem]
[UpdateInGroup(typeof(CommandGroup))]
public partial class RegisterUnselectCommandSystem : GtionSystemBase
{
    EndSimulationEntityCommandBufferSystem ecb;
    EntityQuery commanderQuery;
    EntityQuery unitQuery;

    protected override void OnCreate()
    {
        base.OnCreate();
        var commanderDesc = new EntityQueryDesc
        {
            All = new[] { ComponentType.ReadOnly<UnitOwner>(), ComponentType.ReadOnly<CommanderTag>(), ComponentType.ReadOnly<UnSelectCommand>() },
            None = new[] { ComponentType.ReadOnly<UnitTag>() }
        };
        commanderQuery = GetEntityQuery(commanderDesc);
        unitQuery = GetEntityQuery(ComponentType.ReadOnly<UnitTag>(), ComponentType.ReadOnly<SelectedUnitTag>(), ComponentType.ReadOnly<UnitOwner>());
        
        ecb = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate()
    {
        if (commanderQuery.CalculateEntityCount() == 0) return;

        var commandBuffer = ecb.CreateCommandBuffer();
        var commander = commanderQuery.ToEntityArray(Allocator.Temp);

        for (int i = 0; i < commander.Length; i++)
        {
            UnitOwner ownerData = EntityManager.GetSharedComponentData<UnitOwner>(commander[i]);
            unitQuery.SetSharedComponentFilter(ownerData);

            int entity = unitQuery.CalculateEntityCount();
            if (entity > 0)
            {
                commandBuffer.RemoveComponentForEntityQuery<SelectedUnitTag>(unitQuery);
            }
        }
        commandBuffer.RemoveComponentForEntityQuery<UnSelectCommand>(commanderQuery);
    }
}
