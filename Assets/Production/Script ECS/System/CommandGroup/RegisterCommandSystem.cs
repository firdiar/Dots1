using Gtion.Plugin.DI;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Burst;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;



/// <summary>
/// In this class all player command from player input is being constructed
/// </summary>
[DisableAutoCreation]
public partial class RegisterCommandSystem : GtionSystemBase
{
    [GInject]
    PathFinder pathFinder;

    EntityArchetype playerCommandArch;
    int playerIndexCounter = 0;
    EndSimulationEntityCommandBufferSystem ecb;

    EntityQuery commanderQuery;
    EntityQuery unitQuery;

    protected override void OnCreate()
    {        
        ecb = this.World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
        playerCommandArch = EntityManager.CreateArchetype
                                (
                                    ComponentType.ReadOnly<UnitOwner>(),
                                    ComponentType.ReadOnly<CommanderTag>()
                                );

        var commanderDesc = new EntityQueryDesc
        {
            All = new[] { ComponentType.ReadOnly<UnitOwner>(), ComponentType.ReadOnly<CommanderTag>() },
            None = new[] { ComponentType.ReadOnly<UnitTag>() }
        };

        unitQuery = GetEntityQuery(ComponentType.ReadOnly<UnitTag>(), ComponentType.ReadOnly<SelectedUnitTag>(), ComponentType.ReadOnly<UnitOwner>());
        commanderQuery = GetEntityQuery(commanderDesc);

        base.OnCreate();
    }

    public void RegisterNewPlayer(out Entity entity, out UnitOwner identity) 
    {
        entity = EntityManager.CreateEntity(playerCommandArch);
        identity = new UnitOwner()
        {
            ownerId = playerIndexCounter
        };
        EntityManager.SetSharedComponentData(entity, identity);
        Debug.Log($"#Registered Player ID : {playerIndexCounter}");
        playerIndexCounter++;
    }

    public EntityQuery GetSelectedUnit(UnitOwner identity) 
    {
        var query = GetEntityQuery(ComponentType.ReadOnly<UnitTag>(), ComponentType.ReadOnly<SelectedUnitTag>(), ComponentType.ReadOnly<UnitOwner>());
        query.SetSharedComponentFilter(identity);

        return query;
    }

    protected override void OnUpdate()
    {
        var commanders = commanderQuery.ToEntityArray(Allocator.Temp);
        if (commanders.Length > 0)
        {
            ExecuteCommand(commanders);
        }
        commanders.Dispose();
    }

    void ExecuteCommand(NativeArray<Entity> commanders) 
    {
        var commandBuffer = ecb.CreateCommandBuffer();
        var selectCommand = GetComponentDataFromEntity<SelectCommand>(true);
        var unSelectCommand = GetComponentDataFromEntity<UnSelectCommand>(true);
        var moveCommand = GetComponentDataFromEntity<MoveCommand>(true);

        foreach (var commander in commanders)
        {
            var ownerData = EntityManager.GetSharedComponentData<UnitOwner>(commander);
            if (selectCommand.HasComponent(commander))
            {
                //SelectCommand commandData = EntityManager.GetComponentData<SelectCommand>(commander);
                //ExecuteCommand(commandBuffer, ownerData, commandData);
                //commandBuffer.RemoveComponent<SelectCommand>(commander);
            }

            if (unSelectCommand.HasComponent(commander))
            {
                //UnSelectCommand commandData = EntityManager.GetComponentData<UnSelectCommand>(commander);
                //ExecuteCommand(commandBuffer, ownerData);//, commandData);
                //commandBuffer.RemoveComponent<UnSelectCommand>(commander);
            }

            if (moveCommand.HasComponent(commander))
            {
                //MoveCommand commandData = EntityManager.GetComponentData<MoveCommand>(commander);
                //ExecuteCommand(commandBuffer, ownerData, commandData);
                //commandBuffer.RemoveComponent<MoveCommand>(commander);
            }
        }
    }

    #region Select Command
    void ExecuteCommand(EntityCommandBuffer commandBuffer, UnitOwner ownerData, SelectCommand commandData)
    {
        EntityCommandBuffer.ParallelWriter parallelEcb = commandBuffer.AsParallelWriter();
        Dependency = Entities
            .WithSharedComponentFilter(ownerData)
            .ForEach((Entity unit, int entityInQueryIndex, in Translation translation) =>
            {
                float2 unitPos = translation.Value.XZ();
                bool isSelected =  //checking if unit inside rectangle selected
                        commandData.selectStartPosition.x <= unitPos.x &&
                        commandData.selectStartPosition.y <= unitPos.y &&
                        commandData.selectEndPosition.x >= unitPos.x &&
                        commandData.selectEndPosition.y >= unitPos.y;

                if (isSelected)
                {
                    parallelEcb.AddComponent<SelectedUnitTag>(entityInQueryIndex, unit);
                }
            }).ScheduleParallel(Dependency);
        Dependency.Complete();
    }
    #endregion

    #region Un Select Command
    void ExecuteCommand(EntityCommandBuffer commandBuffer, UnitOwner ownerData)//, UnSelectCommand commandData)
    {
        EntityCommandBuffer.ParallelWriter parallelEcb = commandBuffer.AsParallelWriter();
        Dependency = Entities
            .WithSharedComponentFilter(ownerData)
            .ForEach((Entity unit, int entityInQueryIndex, in SelectedUnitTag translation) =>
            {
                parallelEcb.RemoveComponent<SelectedUnitTag>(entityInQueryIndex, unit);
            }).ScheduleParallel(Dependency);
        Dependency.Complete();
    }
    #endregion

    #region Move Command
    void ExecuteCommand(EntityCommandBuffer commandBuffer, UnitOwner ownerData, MoveCommand commandData)
    {
        UnitPathFinding pathFinding = GeneratePathFinding(commandBuffer , ownerData, commandData.targetPosition);// new UnitPathFinding();// { orderId = commandData.moveOrderId };

        if (pathFinding.orderId < 0)
        {
            unitQuery.ResetFilter();
            unitQuery.SetSharedComponentFilter(ownerData);
            commandBuffer.RemoveComponentForEntityQuery<SelectedUnitTag>(unitQuery);
            return;
        }

        EntityQuery idleUnits = GetEntityQuery(
                        new EntityQueryDesc
                        {
                            All = new[] { ComponentType.ReadOnly<UnitTag>(), ComponentType.ReadOnly<SelectedUnitTag>(), ComponentType.ReadOnly<UnitOwner>() },
                            None = new[] { ComponentType.ReadOnly<UnitPathFinding>() }
                        });
        idleUnits.SetSharedComponentFilter(ownerData);
        AddMoveCommand(commandBuffer, idleUnits, pathFinding);
        

        EntityQuery movingUnits = GetEntityQuery(
                new EntityQueryDesc
                {
                    All = new[] { ComponentType.ReadOnly<UnitPathFinding>(), ComponentType.ReadOnly<UnitTag>(), ComponentType.ReadOnly<SelectedUnitTag>(), ComponentType.ReadOnly<UnitOwner>() }
                });
        

        List<UnitPathFinding> existingOrders = new List<UnitPathFinding>();        
        EntityManager.GetAllUniqueSharedComponentData(existingOrders);
        for(int i = 0; i < existingOrders.Count; i++)
        {
            movingUnits.ResetFilter();
            var existingOrder = existingOrders[i];
            movingUnits.SetSharedComponentFilter(ownerData);
            movingUnits.SetSharedComponentFilter(existingOrder);

            int unitCount = movingUnits.CalculateEntityCount();
            pathFinder.FinishOrder(existingOrder.orderId, unitCount);

            commandBuffer.RemoveComponentForEntityQuery<UnitPathFinding>(movingUnits);
            AddMoveCommand(commandBuffer, movingUnits, pathFinding);
        }
        Debug.Log($"Move Command Done");
    }
    void AddMoveCommand(EntityCommandBuffer commandBuffer , EntityQuery units, UnitPathFinding pathFinding ) 
    {
        commandBuffer.RemoveComponentForEntityQuery<SelectedUnitTag>(units);
        commandBuffer.AddSharedComponentForEntityQuery(units, pathFinding);
    }

    UnitPathFinding GeneratePathFinding(EntityCommandBuffer commandBuffer, UnitOwner owner, float2 targetPosition) 
    {
        unitQuery.SetSharedComponentFilter(owner);
        int entityCount = unitQuery.CalculateEntityCount();

        GridSettings setting = pathFinder.Settings;
        float2 averagePosition = float2.zero;
        HashSet<int> startPoint = new HashSet<int>();
        UnitSizeType[] unitSizeTypes = new UnitSizeType[entityCount];

        NativeArray<SortHelper> entities = new NativeArray<SortHelper>(entityCount , Allocator.Temp);
        Entities
            .WithAll<UnitTag>()
            .WithSharedComponentFilter(owner)
            .WithAll<SelectedUnitTag>()
            .ForEach((Entity entity, int entityInQueryIndex, in Translation translation, in UnitSize unitSize) => 
            {
                unitSizeTypes[entityInQueryIndex] = EntityManager.GetSharedComponentData<UnitSize>(entity).type;
                float2 position = translation.Value.XZ();
                averagePosition += position;
                int gridIndex = setting.GetNodeIndex(position);
                if(!startPoint.Contains(gridIndex))
                {
                    startPoint.Add(gridIndex);
                }

                entities[entityInQueryIndex] = new SortHelper() { entity = entity, position = position };
            }).WithoutBurst().Run();
        averagePosition /= entityCount;

        //Debug.Log("Checking All Entity Done");
        float2 direction = math.normalize(targetPosition - averagePosition);
        ArrangementResult result = UnitPlacementHandler.ArrangePlacement(setting, unitSizeTypes, targetPosition, direction);
        //Debug.Log("Arrange Formation Donw");
        int orderId = pathFinder.RequestOrder(startPoint, result.finishPoint, entityCount);
        //Debug.Log($"Path Finding Doee {orderId}");

        if (orderId >= 0)
        {
            if (entityCount < 1000)
            {
                entities.Sort(new EntitySort() { direction = direction });
            }

            quaternion rotation = quaternion.LookRotation(new float3(direction.x, 0, direction.y), new float3(0, 1, 0));
            for (int i = 0; i < entities.Length; i++)
            {
                commandBuffer.AddComponent(entities[i].entity, new UnitMovementOrder() { targetPosition = result.placement[i], targetRotation = rotation });
            }
        }
        entities.Dispose();

        return new UnitPathFinding() { orderId = orderId };
    }

    public struct SortHelper
    {
        public Entity entity;
        public float2 position;
    }
    public struct EntitySort : IComparer<SortHelper>
    {
        public float2 direction;
        public int Compare(SortHelper a, SortHelper b)
        {
            return a.entity.Index - b.entity.Index;
            /*
            if (Vector2.Angle(direction, Vector2.up) < 90)
            {
                return a.entity.Index - b.entity.Index;
            }
            else
            {
                return b.entity.Index - a.entity.Index;
            }
            */
        }
    }
    #endregion
}
