using Gtion.Plugin.DI;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

public abstract partial class GtionSystemBase : SystemBase
{
    bool isInitialized;

    public bool IsInitialized => isInitialized;

    protected override void OnCreate()
    {
        base.OnCreate();
        GDi.Request(this.GetType(), this, OnDepedencyReady);
    }

    protected virtual void OnDepedencyReady()
    {
        isInitialized = true;
    }
}
