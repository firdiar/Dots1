using Gtion.Plugin.DI;
using System.Collections;
using System.Collections.Generic;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

public partial class AttackingSystem : GtionSystemBase
{
    EntityQuery unitQuery;

    [GInject]
    IMapGrids mapGrids;
    [GInject]
    MersenneTwister random;

    QuadrantGroupSystem groupSystem;
    EndSimulationEntityCommandBufferSystem ecb;
    NativeArray<float2> byteToDirections;
    protected override void OnCreate()
    {
        base.OnCreate();

        ecb = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
        byteToDirections = new NativeArray<float2>(OrderGrids.Directions, Allocator.Persistent);//converter byte to float2
        //only idle unit can attack
        unitQuery = GetEntityQuery(new EntityQueryDesc
        {
            All = new[] { ComponentType.ReadOnly<UnitTag>(), ComponentType.ReadOnly<HasTarget>(), ComponentType.ReadOnly<UnitOwner>(),
                          ComponentType.ReadOnly<UnitAttackStatus>(),ComponentType.ReadOnly<UnitAttackRadius>(), ComponentType.ReadWrite<UnitAttackCooldown>(),
                          ComponentType.ReadOnly<UnitMovementStatus>(), ComponentType.ReadWrite<UnitManuever>(),
            },
            None = new[] { ComponentType.ReadOnly<UnitMovementOrder>() }
        });
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        byteToDirections.Dispose();
    }

    protected override void OnStartRunning()
    {
        base.OnStartRunning();
        groupSystem = World.GetOrCreateSystem<QuadrantGroupSystem>();
    }

    protected override void OnUpdate()
    {
        var commandBuffer = ecb.CreateCommandBuffer();
        //var unitTranslate = GetComponentDataFromEntity<Translation>(true);
        var ltw = GetComponentDataFromEntity<LocalToWorld>(true);
        var unitHealth = GetComponentDataFromEntity<UnitVitalityHealth>(true);

        NativeList<DamageHolderTemp> damageHolders = new NativeList<DamageHolderTemp>(unitQuery.CalculateEntityCount() , Allocator.TempJob);
        BattleWithTargetJob battleWithTargetJob = new BattleWithTargetJob()
        {
            deltaTime = Time.DeltaTime,
            settings = mapGrids.Settings,
            byteToDirection = byteToDirections,

            mapGrids = groupSystem.mapWorld,

            unitTranslations = ltw,
            damageHolders = damageHolders.AsParallelWriter(),

            commandBuffer = commandBuffer.AsParallelWriter()
        };
        Dependency = battleWithTargetJob.ScheduleParallel(unitQuery , Dependency);
        Dependency.Complete();

        NativeParallelHashMap<Entity, int> damageRecords = new NativeParallelHashMap<Entity, int>(damageHolders.Length, Allocator.Temp);
        foreach (var damages in damageHolders)
        {
            if (damageRecords.ContainsKey(damages.entity))
            {
                damageRecords[damages.entity] += damages.damage;
            }
            else
            {
                damageRecords.Add(damages.entity, damages.damage);
            }
        }
        damageHolders.Dispose();

        foreach (var damageRecord in damageRecords)
        {
            if (unitHealth.TryGetComponent(damageRecord.Key, out var vitalityHealth))
            {
                if (vitalityHealth.health > damageRecord.Value)
                {
                    vitalityHealth.health -= damageRecord.Value;
                    Debug.Log($" {damageRecord.Key.Index}:{damageRecord.Key.Version} got {damageRecord.Value} Damage! (HP : {vitalityHealth.health})");
                    //unitHealth[damageRecord.Key] = vitalityHealth;
                    commandBuffer.SetComponent(damageRecord.Key, vitalityHealth);
                }
                else
                {
                    commandBuffer.AddComponent<DestroyHierarchyTag>(damageRecord.Key);
                    //commandBuffer.Add(damageRecord.Key);
                }
            }
        }        
        damageRecords.Dispose();
    }

    public struct DamageHolderTemp
    {
        public Entity entity;
        public int damage;
    }

    [BurstCompile]
    public partial struct BattleWithTargetJob : IJobEntity
    {
        public float deltaTime;
        public GridSettings settings;

        [ReadOnly]
        public NativeArray<float2> byteToDirection;

        [ReadOnly]
        public NativeArray<NodePoint> mapGrids;

        [ReadOnly]
        public ComponentDataFromEntity<LocalToWorld> unitTranslations;
        public NativeList<DamageHolderTemp>.ParallelWriter damageHolders;

        public EntityCommandBuffer.ParallelWriter commandBuffer;

        public void Execute(Entity entity, [EntityInQueryIndex] int sortKey, ref Translation translation,ref Rotation rotation,
            ref UnitAttackCooldown cooldown, ref UnitManuever manuever, in UnitAttackRadius attackRadius, in UnitAttackStatus attackStatus, 
            in HasTarget target, in UnitMovementStatus movementStatus)
        {
            //cooldown
            bool isFireCooldown = cooldown.value > 0;
            if (isFireCooldown)
            {
                cooldown.value -= deltaTime;
            }
            
            if (!unitTranslations.TryGetComponent(target.entity, out LocalToWorld enemyTranslation))
            {
                //enemy has died
                commandBuffer.RemoveComponent<HasTarget>(sortKey, entity);
                return;
            }

            float2 myPos = translation.Value.XZ();
            float2 enemyPos = enemyTranslation.Position.XZ();
            float2 direction = enemyPos - myPos;
            float distance = math.length(direction);
            float2 normalDirection = math.normalize(direction);

            quaternion lookAt = quaternion.LookRotation(normalDirection.X0Y(), new float3(0, 1, 0));

            if (distance <= attackRadius.attack)
            {
                //UnityEngine.Debug.Log($"HereA");
                //do damage if not cooldown
                if (!isFireCooldown)
                {
                    damageHolders.AddNoResize(new DamageHolderTemp() { entity = target.entity, damage = attackStatus.damage });
                    cooldown.value = attackStatus.cooldown;
                    //UnityEngine.Debug.Log("Fire!");
                }
                else
                {
                    //do some manuever
                    float3 updatedPosition = translation.Value + (byteToDirection[manuever.direction] * ((movementStatus.speed/2) * deltaTime)).X0Y();

                    float2 boundPos = new float2(
                        math.clamp(updatedPosition.x, settings.startPoint.x, settings.endPoint.x),
                        math.clamp(updatedPosition.z, settings.startPoint.y, settings.endPoint.y));

                    translation.Value = boundPos.X0Y();

                    manuever.interval -= deltaTime;
                    if (manuever.interval > 0.3f)
                    {
                        lookAt = quaternion.LookRotation(byteToDirection[manuever.direction].X0Y(), new float3(0, 1, 0));
                    }
                    else if (manuever.interval < 0)
                    {
                        manuever.interval = attackStatus.manuverInterval * manuever.random.NextFloat(0.5f, 2f);
                        manuever.direction = (byte)manuever.random.NextInt(1 , 9);
                    }
                }
            }
            else if (distance <= attackRadius.chasing)
            {
               
                //chase if enemy try to escape
                NativeList<int> mapGridRay = new NativeList<int>(8, Allocator.Temp);
                settings.RaycastAll(myPos, enemyPos, mapGridRay);
                bool isChasing = true;
                foreach (int gridIndex in mapGridRay)
                {
                    isChasing = isChasing && !mapGrids[gridIndex].blocked;
                    if (!isChasing) break;
                }

                //UnityEngine.Debug.Log($"HereB - {isChasing}");
                if (isChasing)
                {
                    translation.Value += (normalDirection * (movementStatus.speed * deltaTime)).X0Y();
                }
                else 
                {
                    commandBuffer.RemoveComponent<HasTarget>(sortKey, entity);
                }
                mapGridRay.Dispose();
            }
            rotation.Value = lookAt;
        }
    }
}
