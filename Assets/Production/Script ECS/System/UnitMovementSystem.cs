using Gtion.Plugin.DI;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Collections.LowLevel.Unsafe;
using System.Collections.Generic;

public partial class UnitMovementSystem : GtionSystemBase
{
    const float MinDistance = 0.05f;

    [GInject]
    PathFinder pathFinder;
    EndInitializationEntityCommandBufferSystem ecb;

    List<UnitPathFinding> pathFindingOrders = new List<UnitPathFinding>();
    EntityQuery unitQuery;
    NativeArray<float2> byteToDirections;
    protected override void OnCreate()
    {
        base.OnCreate();
        ecb = this.World.GetOrCreateSystem<EndInitializationEntityCommandBufferSystem>();
        unitQuery = GetEntityQuery(
            ComponentType.ReadOnly<UnitTag>(),
            ComponentType.ReadOnly<UnitOwner>(),
            ComponentType.ReadOnly<UnitPathFinding>(), 
            ComponentType.ReadOnly<UnitMovementStatus>(),
            ComponentType.ReadWrite<Translation>(),
            ComponentType.ReadWrite<Rotation>(),
            ComponentType.ReadWrite<UnitMovementOrder>()
            );

        byteToDirections = new NativeArray<float2>(OrderGrids.Directions, Allocator.Persistent);//converter byte to float2
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        byteToDirections.Dispose();
    }

    protected override void OnUpdate()
    {
        var commandBuffer = ecb.CreateCommandBuffer().AsParallelWriter();
        GridSettings settings = pathFinder.Settings;
        float deltaTime = Time.DeltaTime;
        int totalGrid = settings.TotalGrid;
        

        int lengthDirr = byteToDirections.Length;
        NativeArray<float2>.ReadOnly localByteToDirection = byteToDirections.AsReadOnly();
        pathFindingOrders.Clear();
        EntityManager.GetAllUniqueSharedComponentData(pathFindingOrders);
        foreach (var pathOrder in pathFindingOrders)
        {
            NativeArray<byte> orders = new NativeArray<byte>(pathFinder.Orders.ContainsKey(pathOrder.orderId) ? pathFinder.Orders[pathOrder.orderId].movementOrder : new byte[1], Allocator.TempJob);//array contains Order to move to specific a directions from positions

            unitQuery.SetSharedComponentFilter(pathOrder);
            NativeList<byte> finishedEntity = new NativeList<byte>(unitQuery.CalculateEntityCount(), Allocator.TempJob);
            NativeList<byte>.ParallelWriter finishedEntityParallel = finishedEntity.AsParallelWriter();
            
            Dependency = Entities
                .WithStoreEntityQueryInField(ref unitQuery)
                .WithSharedComponentFilter(pathOrder)
                .WithReadOnly(orders)
                //.WithReadOnly(localByteToDirection)
                .ForEach((Entity entity, int entityInQueryIndex, ref Translation translation, ref Rotation rotation, ref UnitMovementOrder order, in UnitMovementStatus statusMovement) =>
            {
                int index = settings.GetNodeIndex(translation.Value.XZ());
                byte directionByte = orders[index];

                float2 deltaMovement;
                float speed = statusMovement.speed * deltaTime;

                bool isUnknownPoint = directionByte == 0;
                bool isFinishPoint = directionByte == 128;
                bool isBlockedPoint = directionByte == byte.MaxValue;

                if (!isUnknownPoint && !isFinishPoint && !isBlockedPoint) //in flow grid node
                {
                    //UnityEngine.Debug.Log($" DirectionA {isUnknownPoint} : Finish {isFinishPoint} : {order.prevDirection} : {directionByte}");
                    var direction = localByteToDirection[directionByte];
                    deltaMovement = direction * speed;
                    order.prevDirection = directionByte;
                    rotation.Value = quaternion.LookRotation(direction.X0Y(), new float3(0, 1, 0));
                }
                else if (isBlockedPoint && order.prevDirection != 0) //un intentionally goes to blocked node
                {
                    //UnityEngine.Debug.Log($" DirectionB {isUnknownPoint} : Finish {isFinishPoint} : {order.prevDirection} : {directionByte}");
                    var direction = localByteToDirection[order.prevDirection];
                    deltaMovement = direction * speed;
                    rotation.Value = quaternion.LookRotation(direction.X0Y(), new float3(0, 1, 0));
                }
                else if (!isFinishPoint && (isUnknownPoint || isBlockedPoint) && order.prevDirection == 0) //not a finish point but somehow lost
                {
                    //UnityEngine.Debug.Log($" DirectionC {isUnknownPoint} : Finish {isFinishPoint} : {order.prevDirection} : {directionByte}");
                    var unNormalDirection = order.targetPosition - translation.Value.XZ();
                    var direction = math.normalize(unNormalDirection);
                    deltaMovement = direction * speed;
                    rotation.Value = quaternion.LookRotation(direction.X0Y(), new float3(0, 1, 0));
                }
                else //in finished area
                {
                    //UnityEngine.Debug.Log($" DirectionD {isUnknownPoint} : Finish {isFinishPoint} : {order.prevDirection} : {directionByte}");
                    var position = translation.Value.XZ();

                    if (!order.isValid)//run raycast once to make sure our target position is reachable
                    {
                        NativeList<int> grids = new NativeList<int>(16, Allocator.Temp);
                        settings.RaycastAll(position, order.targetPosition, grids);
                        order.isValid = true;
                        foreach (var grid in grids)
                        {
                            if (orders[index] == byte.MaxValue)
                            {
                                order.isValid = false;
                                break;
                            }
                        }
                        grids.Dispose();
                    }

                    var unNormalDirection = order.targetPosition - position;
                    var direction = math.normalize(unNormalDirection);
                    deltaMovement = direction * speed;

                    if (!order.isValid || (unNormalDirection.LengthRect() < MinDistance && math.length(unNormalDirection) < MinDistance))
                    {
                        //on finished normalize facing direction
                        rotation.Value = order.targetRotation;

                        //remove                       
                        commandBuffer.RemoveComponent<UnitPathFinding>(entityInQueryIndex, entity);
                        commandBuffer.RemoveComponent<UnitMovementOrder>(entityInQueryIndex, entity);
                        finishedEntityParallel.AddNoResize(byte.MinValue);
                    }
                    else
                    {
                        rotation.Value = quaternion.LookRotation(direction.X0Y(), new float3(0, 1, 0));
                    }
                }
                translation.Value += deltaMovement.X0Y();

            }).ScheduleParallel(Dependency);
            Dependency.Complete();

            int arrivedEntityCount = finishedEntity.Length;
            if (arrivedEntityCount > 0)
            {
                //UnityEngine.Debug.Log($"Finished Path {pathOrder.orderId} : {arrivedEntityCount}");
                pathFinder.FinishOrder(pathOrder.orderId, arrivedEntityCount);
            }
            finishedEntity.Dispose();
            orders.Dispose();
        }
        //byteToDirections.Dispose();
    }
}
