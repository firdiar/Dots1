using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;
using System.Linq;

public partial class MainGameObjectUpdateTransformSystem : CompanionGameObjectUpdateTransformSystem
{
    

    protected override void OnCreate()
    {
        base.OnCreate();
        World.GetExistingSystem<CompanionGameObjectUpdateTransformSystem>().Enabled = false;
        
        ComponentType playerUpdateTag = ComponentType.ReadOnly<UpdateTransformTag>();
        EntityQuery entityQuery = (EntityQuery)ReflectionsHelper.GetField(this, "m_ExistingQuery");

        var desc = entityQuery.GetEntityQueryDesc();
        var temp = desc.All.ToList();
        temp.Add(playerUpdateTag);
        desc.All = temp.ToArray();

        entityQuery = GetEntityQuery(desc);
        ReflectionsHelper.SetField(this, "m_ExistingQuery", entityQuery);
    }
}
