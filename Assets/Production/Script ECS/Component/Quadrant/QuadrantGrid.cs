using Gtion.Plugin.DI;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IQuadrantGrids
{
    public GridSettings Settings { get; }
}

public class QuadrantGrid : MonoBehaviour, IQuadrantGrids
{
    [RequireType(typeof(IMapGrids))]
    [SerializeField]
    UnityEngine.Object mapGrids;
    [GInject]
    IMapGrids InjectMapGrids;
    IMapGrids MapGrids => (InjectMapGrids == null) ? (mapGrids as IMapGrids) : InjectMapGrids;

    [Header("Config")]
    [SerializeField]
    bool enableGizmo = true;
    [SerializeField]
    bool isDebugQuadrantGrids;
    [SerializeField]
    int quadrantGrid;

    [SerializeField]
    [ReadOnly]
    Vector2 worldSize;
    [SerializeField]
    [ReadOnly]
    GridSettings quadrantSettings;

    public GridSettings Settings => quadrantSettings;

    private void Start()
    {
        if (isDebugQuadrantGrids)
        {
            Destroy(this);
            return;
        }

        GDi.Request(this, OnDepedencyReady);
        GDi.Register<IQuadrantGrids>(this);        
    }

    void OnDepedencyReady() 
    {
        //Debug.Log((InjectMapGrids == null));
        Bake();        
    }

    [Button]
    void Bake() 
    {
        var settings = MapGrids.Settings;
        quadrantSettings = settings;
        worldSize = settings.nodeSize * (Vector2)settings.gridSize;

        //manual gridding
        quadrantSettings.gridSize = new Vector2Int(quadrantGrid, quadrantGrid);
        quadrantSettings.nodeSize = worldSize.x / quadrantGrid;
        quadrantSettings = quadrantSettings.Init();
    }

    public void OnDrawGizmos()
    {
        if (!enableGizmo) return;

        var settings = MapGrids.Settings;
        Debug.DrawRectangle(settings.centerPoint.X0Y(), worldSize, Color.red);
        quadrantSettings.DrawWorld();
    }

    private void OnValidate()
    {
        if (MapGrids == null) return;

        Bake();
    }
}
