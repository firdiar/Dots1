using Unity.Entities;
using Unity.Mathematics;


public struct SelectCommand : IComponentData
{
    public float2 selectStartPosition;
    public float2 selectEndPosition;
}

public struct UnSelectCommand : IComponentData
{
}

public struct MoveCommand : IComponentData
{
    public float2 targetPosition;
}
