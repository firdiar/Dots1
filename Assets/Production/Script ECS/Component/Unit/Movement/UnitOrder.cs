using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

public struct UnitPathFinding : ISharedComponentData
{
    public int orderId;
}

public struct UnitMovementOrder : IComponentData
{
    public float2 targetPosition;
    public quaternion targetRotation;

    public byte prevDirection;
    public bool isValid;
}
