using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

[System.Serializable]
public struct UnitVitalityStatus : IComponentData
{
    public int maxHealth;
}

public struct UnitVitalityHealth : IComponentData
{
    /// <summary>
    /// Actual value of max health
    /// </summary>
    public int health;
}
