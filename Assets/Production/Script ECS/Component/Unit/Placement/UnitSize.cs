using Unity.Entities;

public enum UnitSizeType
{ 
    Infantry = 0,

    Tank = 100,


}

[System.Serializable]
public struct UnitSize : ISharedComponentData
{
    public UnitSizeType type;
}