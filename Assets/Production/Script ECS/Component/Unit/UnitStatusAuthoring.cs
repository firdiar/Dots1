using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;


[DisallowMultipleComponent]
public class UnitStatusAuthoring : MonoBehaviour, IConvertGameObjectToEntity
{
    [Header("Identifier")]
    [SerializeField]
    int ownerId;
    [SerializeField]
    UnitSizeType sizeType;

    [Header("Status Vitality")]
    [SerializeField]
    UnitVitalityStatus vitalityStatus;

    [Header("Status Movement")]
    [SerializeField]
    UnitMovementStatus movementStatus;

    [Header("Status Attack")]
    [SerializeField]
    UnitAttackRadius attackRadius;
    [SerializeField]
    UnitAttackStatus attackStatus;

    [Header("Editor Only")]
    [SerializeField]
    UnitSizeGroupingConfig config;
    [SerializeField]
    bool showSizeIndicator;

    [SerializeField]
    bool showAttackRadius;

    public UnitOwner Owner => new UnitOwner() { ownerId = ownerId };
    public UnitAttackRadius AttackRadius => attackRadius;
    public UnitSizeType SizeType => sizeType;


    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        dstManager.AddSharedComponentData(entity, Owner);        
        dstManager.AddSharedComponentData(entity, new UnitSize() { type = SizeType });
        
        dstManager.AddComponentData(entity, movementStatus);

        dstManager.AddComponentData(entity, AttackRadius);
        dstManager.AddComponentData(entity, attackStatus);
        dstManager.AddComponentData(entity, new UnitAttackCooldown());
        dstManager.AddComponentData(entity, new UnitManuever() { random = Unity.Mathematics.Random.CreateFromIndex((uint)entity.Index) });

        dstManager.AddComponentData(entity, vitalityStatus);
        dstManager.AddComponentData(entity, new UnitVitalityHealth() { health = vitalityStatus.maxHealth });


    }

    private void OnDrawGizmos()
    {
        if (config == null) return;

        if (showSizeIndicator)
        {
            Debug.DrawRectangle(transform.position, config[SizeType], Color.blue);
        }

        if (showAttackRadius)
        {
            Debug.DrawCircle(transform.position, AttackRadius.attack, Color.red);
            Debug.DrawCircle(transform.position, AttackRadius.detection, Color.gray);
            Debug.DrawCircle(transform.position, AttackRadius.chasing, Color.white);
        }
    }

    private void OnValidate()
    {
        //Attack Radius
        attackRadius.attack = Mathf.Clamp(attackRadius.attack, 0, attackRadius.detection);
        attackRadius.detection = Mathf.Clamp(attackRadius.detection, attackRadius.attack, attackRadius.chasing);
    }
}
