using Unity.Entities;
using Unity.Mathematics;

[System.Serializable]
public struct UnitAttackRadius : IComponentData
{
    public float chasing;
    public float detection;
    public float attack;
}

[System.Serializable]
public struct UnitAttackStatus : IComponentData
{
    public int damage;
    public float cooldown;
    public float manuverInterval;
}

[System.Serializable]
public struct UnitAttackCooldown : IComponentData
{
    public float value;
}

[System.Serializable]
public struct UnitManuever : IComponentData
{
    public Random random;
    public byte direction;
    public float interval;
}