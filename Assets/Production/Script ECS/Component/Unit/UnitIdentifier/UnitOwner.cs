using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;


[System.Serializable]
public struct UnitOwner : ISharedComponentData
{
    public int ownerId;
}
