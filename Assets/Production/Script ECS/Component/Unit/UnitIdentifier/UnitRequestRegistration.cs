using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

//register UnitOwner on start
[GenerateAuthoringComponent]
public struct RequestUnitRegistration : IComponentData
{
    public int ownerId;
}
