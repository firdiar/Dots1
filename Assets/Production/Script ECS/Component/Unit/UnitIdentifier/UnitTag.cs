using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

//basically all unit have to implement this tag
[GenerateAuthoringComponent]
public struct UnitTag : IComponentData
{
}
