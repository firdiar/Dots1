#if UNITY_EDITOR
using System;
using System.Linq;
using UnityEditor;
#endif
using UnityEngine;

// Created by V(Firdi) aka Firdiansyah Ramadhan

/// <summary>
/// Attribute that require implementation of the provided type.
/// </summary>
public class RequireTypeAttribute : PropertyAttribute
{
    // Interface type.
    public System.Type[] requiredType { get; private set; }

    /// <summary>
    /// Requiring implementation of the <see cref="T:RequireInterfaceAttribute"/> interface.
    /// </summary>
    /// <param name="type">Interface type.</param>
    public RequireTypeAttribute(params System.Type[] type)
    {
        this.requiredType = type;
    }
}

#if UNITY_EDITOR
/// <summary>
/// Drawer for the RequireInterface attribute.
/// </summary>
[CustomPropertyDrawer(typeof(RequireTypeAttribute))]
public class RequireTypeDrawer : PropertyDrawer
{
    public Type[] GetInterfacesImplementation(Type type)
    {
        return type.GetInterfaces();
    }

    /// <summary>
    /// Overrides GUI drawing for the attribute.
    /// </summary>
    /// <param name="position">Position.</param>
    /// <param name="property">Property.</param>
    /// <param name="label">Label.</param>
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {

        // Check if this is reference type property.
        if (property.propertyType == SerializedPropertyType.ObjectReference)
        {
            // Get attribute parameters.
            var requiredAttribute = this.attribute as RequireTypeAttribute;

            if (requiredAttribute.requiredType.Length > 1)
            {
                label.text += " ( " + requiredAttribute.requiredType.Length + " Type)";
            }
            else if (requiredAttribute.requiredType.Length == 1)
            {
                label.text += $" ({requiredAttribute.requiredType[0].Name})";
            }
            else
            {
                label.text += " ( UNDEFINED Type)";
            }

            // Begin drawing property field.
            EditorGUI.BeginProperty(position, label, property);

            // Draw property field.
            UnityEngine.Object obj = EditorGUI.ObjectField(position, label, property.objectReferenceValue, typeof(UnityEngine.Object), true);
            
            
            if (obj is GameObject g)
            {          
                bool found;
                var components = g.GetComponents<Component>();
                for(int i = 0; i < components.Length; i++)
                {
                    var comp = components[i];
                    found = true;
                    var types = GetInterfacesImplementation(comp.GetType());
                    foreach (var requirement in requiredAttribute.requiredType)
                    {
                        if (!types.Contains(requirement)) 
                        {
                            found = false;
                            break;
                        }
                    }
                    //Debug.Log(comp.GetType() + " - " + (found));
                    if (found)
                    {
                        property.objectReferenceValue = components[i];
                        break;
                    }
                    else
                    {
                        property.objectReferenceValue = null;
                    }    
                }               
            }            

            if (obj == null) property.objectReferenceValue = null;

            // Finish drawing property field.
            EditorGUI.EndProperty();
        }
        else
        {
            // If field is not reference, show error message.
            // Save previous color and change GUI to red.
            var previousColor = GUI.color;
            GUI.color = Color.red;

            // Display label with error message.
            EditorGUI.LabelField(position, label, new GUIContent("Property is not a reference type"));

            // Revert color change.
            GUI.color = previousColor;
        }
    }
}
#endif